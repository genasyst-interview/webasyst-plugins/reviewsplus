(function($) {
    function empty( mixed_var ) {   // Determine whether a variable is empty
        return ( mixed_var === "" || mixed_var === 0
        || mixed_var === "0" || mixed_var === null  ||
        mixed_var === false  ||
        ( typeof(mixed_var)=='object' && mixed_var.length === 0 ) );
    }
    function var_dump () {
        var output = '', pad_char = ' ', pad_val = 4, lgth = 0, i = 0, d = this.window.document;
        var getFuncName = function (fn) {
            var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
            if (!name) {
                return '(Anonymous)';
            }
            return name[1];
        };
        var repeat_char = function (len, pad_char) {
            var str = '';
            for (var i=0; i < len; i++) {
                str += pad_char;
            }
            return str;
        };
        var getScalarVal = function (val) {
            var ret = '';
            if (val === null) {
                ret = 'NULL';
            } else if (typeof val === 'boolean') {
                ret = 'bool(' + val + ')';
            } else if (typeof val === 'string') {
                ret = 'string(' + val.length + ') "' + val + '"';
            } else if (typeof val === 'number') {
                if (parseFloat(val) == parseInt(val, 10)) {
                    ret = 'int(' + val + ')';
                } else {
                    ret = 'float(' + val + ')';
                }
            } else if (val === undefined) {
                ret = 'UNDEFINED'; // Not PHP behavior, but neither is undefined as value
            }  else if (typeof val === 'function') {
                ret = 'FUNCTION'; // Not PHP behavior, but neither is function as value
                ret = val.toString().split("\n");
                txt = '';
                for(var j in ret) {
                    txt += (j !=0 ? thick_pad : '') + ret[j] + "\n";
                }
                ret = txt;
            } else if (val instanceof Date) {
                val = val.toString();
                ret = 'string('+val.length+') "' + val + '"'
            }
            else if(val.nodeName) {
                ret = 'HTMLElement("' + val.nodeName.toLowerCase() + '")';
            }
            return ret;
        };
        var formatArray = function (obj, cur_depth, pad_val, pad_char) {
            var someProp = '';
            if (cur_depth > 0) {
                cur_depth++;
            }
            base_pad = repeat_char(pad_val * (cur_depth - 1), pad_char);
            thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
            var str = '';
            var val = '';
            if (typeof obj === 'object' && obj !== null) {
                if (obj.constructor && getFuncName(obj.constructor) === 'PHPJS_Resource') {
                    return obj.var_dump();
                }
                lgth = 0;
                for (someProp in obj) {
                    lgth++;
                }
                str += "array(" + lgth + ") {\n";
                for (var key in obj) {
                    if (typeof obj[key] === 'object' && obj[key] !== null && !(obj[key] instanceof Date) && !obj[key].nodeName) {
                        str += thick_pad + "["+key+"] =>\n" + thick_pad+formatArray(obj[key], cur_depth+1, pad_val, pad_char);
                    } else {
                        val = getScalarVal(obj[key]);
                        str += thick_pad + "["+key+"] =>\n" + thick_pad + val + "\n";
                    }
                }
                str += base_pad + "}\n";
            } else {
                str = getScalarVal(obj);
            }
            return str;
        };
        output = formatArray(arguments[0], 0, pad_val, pad_char);
        for ( i=1; i < arguments.length; i++ ) {
            output += '\n' + formatArray(arguments[i], 0, pad_val, pad_char);
        }
        return output;
    }
    function av(data) {
        alert(var_dump(data));
    }
    function count(mixed_var, mode) {
        var key, cnt = 0;
        if(mode == 'COUNT_RECURSIVE') mode = 1;
        if(mode != 1) mode = 0;
        for (key in mixed_var){
            cnt++;
            if(mixed_var.hasOwnProperty(key) &&  mode==1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object) ){
                cnt += count(mixed_var[key], 1);
            }
        }
        return cnt;
    }
    $.shopReviewsplusPluginEntity = {
        Field_types: {
            'input':{'name':'Текстовое поле'}
        },
        fields_keys: {},
        create_url: '?plugin=reviewsplus&action=EntityCreate',
        Fields : {
            Dialog: {
                dialog: null,
                entity: null,
                success : function (data) {

                },
                fields: {
                    'id' : '',
                    'entity' : '',
                    'entity_id' : '',
                    'name' :  '',
                    'type' : 'input',
                    'required' : '1',
                    'show' : '1'
                },
                getContainer: function() {
                    if(this.dialog === null) {
                        this.dialog  = $('#reviewsplus-field-dialog');
                    }
                    return  this.dialog;
                },
                clean:function () {
                    this.getContainer().find('h2').html('');
                    this.getContainer().find('.reviewsplus-field-type-static').hide();
                    this.getContainer().find('.reviewsplus-field-key-static').hide();
                    for(var k in this.fields) {
                        this.getContainer().find('#reviewsplus-field-'+k).val(this.fields[k]);
                        this.getContainer().find('#reviewsplus-field-'+k).closest('.field').show();
                    }
                },
                addAction: function(entity)  {
                    this.clean();
                    this.entity = entity;
                    this.set('entity', entity.data('entity'));
                    this.set('entity_id', entity.data('entity_id'));

                    this.getContainer().find('#reviewsplus-field-type').val('input');
                    this.getContainer().find('h2').html('Добавление нового поля');
                    this.setFieldSelect();
                    var type = this.getContainer().find('#reviewsplus-field-type').val();
                    var values = $.shopReviewsplusPluginEntity.getEntityKeyTypeValues(this.entity,type);
                    if(count(values)>0) {
                        this.setFieldKeyType(1);
                    } else {
                        this.setFieldKeyType(2);
                    }
                    this.success = function (data) {
                        var fields_container = entity.find('.reviewsplus-entity-fields-table').find('tbody');
                        var html = $.shopReviewsplusPluginEntity.Fields.getTableFieldHtml(data.field,true);
                        $.shopReviewsplusPluginEntity.addFieldKey(data.field);

                        var html = $(html);
                        fields_container.append(html);
                        setTimeout(function () {
                            html.css({'background-color':'#ff9393', 'transition':'2s cubic-bezier(0.29, 1.89, 1, 1.26)'});
                        },50);
                        setTimeout(function () {
                            html.css('background', ''); 
                            html.css('transition', '');
                        },5000);
                    };
                    this.show();
                },

                editAction: function(field_container) {
                    this.clean();

                    var id = field_container.data('id');
                    if(!empty(id)) {
                        this.getContainer().find('h2').html('Добавление нового поля');
                        for(var k in this.fields){
                           var  val = field_container.data(k);
                            if(typeof (val) != 'undefined') {
                                val = val.toString();
                            }
                            this.set(k, val);
                        }
                        this.getContainer().find('#reviewsplus-field-type').closest('.field').hide();
                        this.getContainer().find('#reviewsplus-field-key').closest('.field').hide();
                        this.getContainer().find('.reviewsplus-field-type-static').find('.value').html(field_container.data('type')).show();
                        this.getContainer().find('.reviewsplus-field-key-static').find('.value').html(field_container.data('key')).show();
                        this.getContainer().find('.reviewsplus-field-type-static').show();
                        this.getContainer().find('.reviewsplus-field-key-static').show();
                    } else {
                       alert('Ошибка: Не передан идентификатор поля!');
                        return;
                    }
                    this.success = function (data) {
                        field_container.data('name',data.field['name']);
                        field_container.data('required',data.field['required']);
                        field_container.data('show', data.field['show']);
                        field_container.find('.reviewsplus-entity-field-name').html(data.field['name']);
                        field_container.find('.reviewsplus-entity-field-required').html(
                            $.shopReviewsplusPluginEntity.Fields.getRequiredHtml(data.field['required']));
                        field_container.find('.reviewsplus-entity-field-show').html(
                            $.shopReviewsplusPluginEntity.Fields.getShowHtml(data.field['show']));


                    };
                    this.show();
                },
                setFieldType: function () {
                    this.setFieldSelect();
                    var type = this.getContainer().find('#reviewsplus-field-type').val();
                    var values = $.shopReviewsplusPluginEntity.getEntityKeyTypeValues(this.entity, type);
                    if(count(values)>0) {
                        this.setFieldKeyType(1);
                    } else {
                        this.setFieldKeyType(2);
                    }

                },
                toggleFieldKeyType: function () {
                    var container =   this.getContainer().find('.reviewsplus-field-key');
                    var input_container = container.find('.reviewsplus-field-key-new');
                    var input = input_container.find('input');
                    if(input.prop('disabled')) {
                        this.setFieldKeyType(2);
                    } else {
                        this.setFieldKeyType(1);
                    }
                },
                setFieldSelect: function () {
                    this.getContainer().find('.reviewsplus-field-key-select-container').html('');
                    var type = this.getContainer().find('#reviewsplus-field-type').val();
                    var values = $.shopReviewsplusPluginEntity.getEntityKeyTypeValues(this.entity,type);
                    html = '';
                    if( count(values) > 0) {
                        html += '<select name="field[key]">';
                        for(var key in values) {
                            var val = values[key];
                            html += '<option value="'+key+'">'+val+'</option>';
                        }
                        html += '</select>';

                    } else {
                        html += 'Ранее созданные поля для данного типа не найдены <a href="#" class="reviewsplus-field-key-type-toggle">Создать новый ключ поля</a>';
                    }
                    this.getContainer().find('.reviewsplus-field-key-select-container').html(html);
                },

                setFieldKeyType: function (type) {
                    var container =   this.getContainer().find('.reviewsplus-field-key');
                    var select_container = container.find('.reviewsplus-field-key-select');
                    var select = container.find('select');
                    var input_container = container.find('.reviewsplus-field-key-new');
                    var input = input_container.find('input');
                    if(type == 1) {
                        input.attr('disabled',true);
                        input_container.hide();

                        select.removeAttr('disabled');
                        select_container.show();

                        container.show();
                    } else if(type == 2) {
                        select.attr('disabled',true);
                        select_container.hide();

                        input.removeAttr('disabled');
                        input_container.show();

                        container.show();
                    } else {
                        input.attr('disabled',true);
                        input_container.hide();
                        select.attr('disabled',true);
                        select_container.hide();
                        container.hide();
                    }
                },
                set: function (name, value) {
                    value = value || '';
                    if(typeof value != 'string') {
                        value = value.toString();
                    }
                    this.getContainer().find('#reviewsplus-field-'+name).val(value);
                },
                show :  function () {
                    var buttons = '<input type="submit" class="button yellow" value="Сохранить"/>&nbsp;&nbsp;или&nbsp;<a href="#" class="cancel">Отмена</a>&nbsp;<span class="reviewsplus-dialog-error"></span>';
                    this.getContainer().waDialog({
                        //disableButtonsOnSubmit: true,
                        'buttons': buttons,
                        onLoad: function () {
                            var self = $(this);

                        },
                        onSubmit: function (dialog) {
                            var f = $(this);
                            f.find('.dialog-buttons i.loading').show();
                            $.post('?plugin=reviewsplus&action=FieldSave', f.serialize(), function (response) {
                                if (response.status == 'ok') {
                                    $.shopReviewsplusPluginEntity.Fields.Dialog.success(response.data);
                                    dialog.trigger('close');
                                } else if (response.status == 'fail') {
                                    dialog.find('.reviewsplus-dialog-error').html(response.errors.join(' '));

                                }
                            }, "json");
                            return false;
                        }
                    });
                    this.getContainer().show();
                },
                hide : function () {
                    this.getContainer().hide();
                    this.clean();
                }
            },
            getTableFieldHtml: function (data) {
                var html = '';
                if(typeof (data)=='object' && count(data)>0 ) {
                    var field = data;
                    var Field_types = $.shopReviewsplusPluginEntity.Field_types;
                    var field_type = $.shopReviewsplusPluginEntity.Field_types['input'];
                    if(Field_types.hasOwnProperty(field['type'])) {
                        field_type = Field_types[field['type']];
                    }
                    var required = this.getRequiredHtml(field['required']) ;
                    var show = this.getShowHtml(field['show']) ;

                    if (field['func']=='dop') {
                        var del  = '<a href="#" class="reviewsplus-entity-field-delete"><i class="icon16 delete"></i></a>';
                    } else {
                        var del  = '<i class="icon16 lock-bw" title="Доступно только скрытие поля"></i>';
                    }
                    html += '<tr class="reviewsplus-entity-field" ' +
                        ' data-id="'+field['id']+'" ' +
                        ' data-name="'+field['name']+'" ' +
                        ' data-key="'+field['key']+'" ' +
                        ' data-type="'+field['type']+'" ' +
                        ' data-required="'+field['required']+'" ' +
                        ' data-show="'+field['show']+'" >' +

                        '<td><i class="icon16 sort"></i></td>' +
                        '<td class="reviewsplus-entity-field-name">'+field['name']+'</td>' +
                        '<td>'+field_type['name']+'</td>' +
                        '<td><a href="#" class="reviewsplus-entity-field-edit"><i class="icon16 edit-bw"></i></a></td>' +
                        '<td  class="reviewsplus-entity-field-required">'+required+'</td>' +
                        '<td  class="reviewsplus-entity-field-show">'+show+'</td>' +
                        '<td>'+del+'</td>' +
                        '</tr>' ;
                }
                return html;

            },
            add: function ($element) {
                var entity_container = $element.closest('.reviewsplus-entity');
                this.Dialog.addAction(entity_container);

            },
            edit : function($element)  {
                var field_container = $element.closest('.reviewsplus-entity-field');
                this.Dialog.editAction(field_container);
            },

            delete : function($element) {
               var field_container = $element.closest('.reviewsplus-entity-field');
                var id = field_container.data('id');
                var entity_container = field_container.closest('.reviewsplus-entity');
                var entity = $.shopReviewsplusPluginEntity.getEntityData(entity_container);
                if(parseInt(id)>0) {
                    if(confirm('Вы точно хотите удалить поле '+field_container.data('name')) ) {
                        $.post('?plugin=reviewsplus&action=FieldDelete', {'id':id,'entity': entity['entity'],'entity_id': entity['entity_id']}, function (response) {
                            if (response.status == 'ok') {
                                field_container.html('<td colspan="7" style="text-align: center; font-weight: bold; background-color:#fff3ba;">Поле удалено</td>');
                                setTimeout(function () {
                                    field_container.fadeOut('slow', function () {
                                        $(this).remove();
                                    });
                                },700);
                            } else if (response.status == 'fail') {
                                alert(response.errors.join(' '));
                            }
                        }, "json");
                        return false;
                    }
                } else {
                    alert('Передан неправильный id!');
                }
            },
            getRequiredHtml: function (val) {
               return (parseInt(val)>0)?
                    '<i class="icon16 status-blue" title="Да"></i>' :'<i class="icon16 status-gray" title="Нет"></i>' ;
            },
            getShowHtml: function (val) {
                return (parseInt(val)>0)?
                    '<i class="icon16 status-green" title="Да"></i>' :'<i class="icon16 status-gray" title="Нет"></i>' ;
            }
        },
        getEntityKeyTypeValues: function (entity, type) {
            var result = {};
            if((typeof (entity) == 'object') && $.shopReviewsplusPluginEntity.fields_keys.hasOwnProperty(type)) {
                var entity_keys = this.getFieldsKeys(entity);
                var fields_values = $.shopReviewsplusPluginEntity.fields_keys[type];
                var field_types = $.shopReviewsplusPluginEntity.Field_types;
                for(var key in fields_values) {
                    var val = fields_values[key];
                    if(!entity_keys.hasOwnProperty(key)) {
                        var type = '';
                        if(field_types.hasOwnProperty(val['type'])) {
                            result[key] = val['name']+' ['+val['key']+']';
                        }
                    }
                }
            }
            return result;
        },
        addFieldKey: function (data) {
            if(!$.shopReviewsplusPluginEntity.fields_keys.hasOwnProperty(data['type'])){
                $.shopReviewsplusPluginEntity.fields_keys[data['type']] = {};
            }
            if(!$.shopReviewsplusPluginEntity.fields_keys[data['type']].hasOwnProperty(data['key'])){
                $.shopReviewsplusPluginEntity.fields_keys[data['type']][data['key']] = {
                    'key': data['key'],
                    'name': data['name'],
                    'type': data['type']
                };
            }
        },
        getFieldsKeys: function (entity) {
            var keys = {};
            entity.find('.reviewsplus-entity-field').each(function () {
                var key = $(this).data('key');
                 keys[key] = $(this).data('type');
            });
            return keys;
        },
        Add: function($element) {
            var entity = $element.data('entity');
            var entity_id = $element.data('entity_id');
            var name = $element.data('entity_name');
            if(!empty(entity)) {
                var data = {
                    'entity' : entity,
                    'entity_id': entity_id,
                    'name' : name
                };
                this.create($element, data);
            } else {
                // DIalog
            }


        },
        getEntityData: function (entity) {
            var r = {
                'entity': entity.data('entity'),
                'entity_id': entity.data('entity_id')
            };
            return r;
        },
        create: function($element, data)  {
            var self = this;
            var entities = $element.closest('.reviewsplus-entities');
            var entities_container = entities.find('.reviewsplus-entities-container');

            $.post(this.create_url, data, function(r) {
                if (r.status == 'ok' && r.data) {
                    var data = r.data;
                    console.log(r.data);
                    var table = $('<table class="light reviewsplus-entity-fields-table"></table>');
                    table.append('<thead>' +
                        '<tr>' +
                        ' <th class="min-width" ></th>' +
                        '<th>Имя</th>' +
                        '<th>Тип</th>' +
                        '<th></th>' +
                        '<th>Обязательное</th>' +
                        '<th>Показывать</th>' +
                        '<th>Удалить</th>' +
                        '</tr>' +
                        '</thead>');

                    if(data.hasOwnProperty('entity') && data.entity.hasOwnProperty('fields') && typeof(data.entity['fields'])=='object') {
                        var html = '<tbody>';
                        var fields = data.entity['fields'];
                        for(var id in fields) {
                            var field = fields[id];
                            html += $.shopReviewsplusPluginEntity.Fields.getTableFieldHtml(field);
                        }
                         html += '</tbody>';
                        table.append(html);
                    }
                    table.append('<tfoot><tr><td colspan="7" style="text-align: center;">' +
                        '<a href="#" class="reviewsplus-entity-fields-add button blue">Добавить поле</a>' +
                        '</td></tr></tfoot>');

                    table.sortable({
                        'distance': 5,
                        'opacity': 0.75,
                        'items': '> tbody:first > tr:visible',
                        'handle': '.sort',
                        'cursor': 'move',
                        'tolerance': 'pointer',
                        'start': function () {
                            $('.block.drop-target').addClass('drag-active');
                        },
                        'stop': function (e,ui) {
                            var success = function(response) {

                            };
                            var item = ui.item;
                            var item_after = item.prev();
                            var data = {'id':item.data('id'),'after_id':item_after.data('id')};
                            $.post('?module=costs&action=materialsSort',{'id':item.data('id'),'after_id':item_after.data('id')}, success, 'json');
                            $('.block.drop-target').removeClass('drag-active');

                        }
                    });
                    var container = $('<div class="field reviewsplus-entity" data-entity="'+data.entity['entity']+'"' +
                        ' data-entity_id="'+data.entity["entity_id"]+'" data-entity_name="'+data.entity['name']+'" style="display: none;">');
                    container.append('<h3>'+data.entity["name"]+'</h3>');
                    var fields_container = $('<div class="reviewsplus-entity-fields"></div>');
                    fields_container.append(table);
                    container.append(fields_container);
                    container.append('<div class="reviewsplus-entity-toggle-fields">' +
                        '<a href="#" class="inline reviewsplus-entity-toggle-fields-link"><i class="icon16 settings"></i>Настройки</a>' +
                        '</div>');
                    entities_container.append(container);
                    if(data.entity['entity'] == 'product') {
                        entities.find('.reviewsplus-entity-create-container').hide().remove();
                    }
                    container.slideDown();
                } else {
                    alert(r.errors.join(','));
                }
            }, 'json');
        },
        Edit: function(data) {

        },
        Delete : function(data) {

        }

    };
    $.shopReviewsplusPluginSettings = {

    };
    $(document).ready(function () {
        
        $('.reviewsplus-entity-fields-table').sortable({
            'distance': 5,
            'opacity': 0.75,
            'items': '> tbody:first > tr:visible',
            'handle': '.sort',
            'cursor': 'move',
            'tolerance': 'pointer',
            'start': function () {
                $('.block.drop-target').addClass('drag-active');
            },
            'stop': function (e,ui) {
                var success = function(response) {

                };
                var item = ui.item;
                var item_after = item.prev();
                var data = {'id':item.data('id'),'after_id':item_after.data('id')};
                $.post('?module=costs&action=materialsSort',{'id':item.data('id'),'after_id':item_after.data('id')}, success, 'json');
                $('.block.drop-target').removeClass('drag-active');

            }
        });
        $('.reviewsplus-entity-create').click(function () {
            $.shopReviewsplusPluginEntity.Add($(this));
            return false;
        });
        $(document).off('click','.reviewsplus-entity-toggle-fields-link').on('click','.reviewsplus-entity-toggle-fields-link' , function () {
           var table =  $(this).closest('.reviewsplus-entity').find('.reviewsplus-entity-fields-table');
           if(table.css('display')=='none')  {
               table.slideDown(500);
           } else {
               table.slideUp(300);
           }
            return false;
        });

        $(document).off('change', '.reviewsplus-field-type').on('change','.reviewsplus-field-type' , function () {
            $.shopReviewsplusPluginEntity.Fields.Dialog.setFieldType($(this));
        });
        $(document).off('click','.reviewsplus-field-key-type-toggle').on('click','.reviewsplus-field-key-type-toggle' , function () {
            $.shopReviewsplusPluginEntity.Fields.Dialog.toggleFieldKeyType($(this));
            return false;
        });
        $(document).off('click','.reviewsplus-entity-field-edit').on('click','.reviewsplus-entity-field-edit' , function () {
            $.shopReviewsplusPluginEntity.Fields.edit($(this));
            return false;
        });
        $(document).off('click','.reviewsplus-entity-fields-add').on('click','.reviewsplus-entity-fields-add' , function () {
            $.shopReviewsplusPluginEntity.Fields.add($(this));
            return false;
        });
        $(document).off('click','.reviewsplus-entity-field-delete').on('click','.reviewsplus-entity-field-delete' , function () {
            $.shopReviewsplusPluginEntity.Fields.delete($(this));
            return false;
        });


    });
})(jQuery);