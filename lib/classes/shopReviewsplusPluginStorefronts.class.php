<?php
class shopReviewsplusPluginStorefronts {

    protected static $storefront = null;
    public static function getStorefronts($return_object = false)  {
        $routing = wa()->getRouting();
        $storefronts = array();
        $domains = $routing->getByApp('shop');
        // Пробегаем по доменам
        foreach ($domains as $domain => $domain_routes) {
            // Забираем все отдельные поселения
            foreach ($domain_routes as $route)  {
                $storefront = $domain.'/'.$route['url'];
                if($return_object) {
                    $storefronts[$storefront] =  new shopReviewsplusPluginStorefront($storefront);
                } else {
                    $storefronts[$storefront] = $storefront;
                }
            }
        }
        return $storefronts;
    }
    public static function getThemes() {
        return wa()->getThemes('shop');
    }
    public static function getStorefrontCode($storefront_name = '') {
        return md5($storefront_name);
    }
    public static function getStorefront($storefront = null) {
        if($storefront!==null) {
            self::$storefront = new shopReviewsplusPluginStorefront($storefront);
        } elseif(self::$storefront==null) {
            $routing = wa()->getRouting();
            $domain = $routing->getDomain();
            $route = $routing->getRoute();
            $storefronts = self::getStorefronts();
            $currentRouteUrl = $domain.'/'.$route['url'];
            $storefront = in_array($currentRouteUrl, $storefronts) ? $currentRouteUrl : '';
            self::$storefront = new shopReviewsplusPluginStorefront($storefront);
        }
        return  self::$storefront;
    }
}