<?php


class shopReviewsplusPluginHelper {

    protected static $entitys = array(
        'product'  =>  'product',
        'storefront'  =>  'storefront',
        'custom'  => 'custom',
    );
    protected static $field_types = array(
        'rate' => array(
            'name' => 'Поле рейтинга (Оценка)',
            'sql_type' => 'TINYINT(2)',
            'default' => '',
        ),
        'input' => array(
            'name' => 'Однострочное текстовое поле (Input)',
            'sql_type' => 'VARCHAR(250)',
            'default' => '',
        ),
        'email' => array(
            'name' => 'Поле ввода Email',
            'sql_type' => 'VARCHAR(250)',
            'default' => '',
        ),
        'textarea' => array(
            'name' =>  'Многострочное текстовое поле (Комментарий)',
            'sql_type' => 'MEDIUMTEXT',
            'default' => '',
        ),
        'images' => array(
            'name' => 'Добавление изображений',
            'sql_type' => 'TINYINT(2)',
            'default' => '',
        ),
        'files' => array(
            'name' => 'Произвольные файлы',
            'sql_type' => 'TINYINT(2)',
            'default' => '',
        ),
        /* Будет когда-нибудь
          'html'  => 'Поле с визуальным редактором', // Если сделают класс фильтра html в фреймворк
          'select'   => 'Выбор из вариантов (Select)',
          'radio'   => 'Выбор из вариантов (Radio кнопки)',
         */
    );
    protected static $base_fields = array(
        'title' => array(
            'key' => 'title',
            'name' => 'Заголовок',
            'type' => 'input',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),
        'site' =>  array(
            'key' => 'site',
            'name' => 'Сайт',
            'type' => 'input',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),
        'rate' =>  array(
            'key' => 'rate',
            'name' => 'Общая оценка',
            'type' => 'rate',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),
        'name' =>  array(
            'key' => 'name',
            'name' => 'Имя',
            'type' => 'input',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),
        'email' =>  array(
            'key' => 'email',
            'name' => 'Email',
            'type' => 'email',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),
        'text' =>  array(
            'key' => 'text',
            'name' => 'Комментарий',
            'type' => 'textarea',
            'required' => 1,
            'show' => 1,
            'delete' => 0,
            'func' => 'main'/* DEPRECATED */
        ),

    );
    protected static $banned_field_keys = array(
        'key',
        'title',
        'site',
        'rate',
        'name',
        'email',
        'text',
        'comments',
        'contact_id',
        'author',
        'datetime_ts',
        'is_new',
        'datetime',
    );
    /* Возвращает типы полей  */
    public static function getFieldTypes() {
        return self::$field_types;
    }
    public static function isFileType($type = '') {
        return ($type == 'files' || $type == 'images')? true : false;
    }
    public static function getBaseFields(){
        return self::$base_fields;
    }
    public static function entityExists($entity) {
        return array_key_exists(strtolower($entity),self::$entitys);
    }
    public static function getPostEntity() {
        $entity = 'product';
        if(waRequest::issetPost('entity'))  {
            $entity = waRequest::post('entity', '',  waRequest::TYPE_STRING_TRIM);
            if(empty($entity)) {
                throw new waException('28171: Передан пустой идентификатор типа отзывов!');
            }
        }
        return $entity;
    }
    public static  function getPostEntityId()  {
        $entity_id = null;
        if(waRequest::issetPost('entity_id')) {
            $_entity_id = waRequest::post('entity_id','', waRequest::TYPE_STRING_TRIM);
            $entity_id = self::castEntityId($_entity_id);
        }
        return $entity_id;
    }
    public static function castEntityId($entity_id = null) {
        if(wa_is_int($entity_id)) {
            return intval($entity_id);
        } elseif(!empty($entity_id)) {
            return $entity_id;
        }
        return null;
    }
}