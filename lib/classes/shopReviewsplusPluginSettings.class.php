<?php

/**
 * Объект витрины
 * Class shopReviewsplusPluginStorefront
 * Добрый день! Прошу предварительно рассмотреть возможность добавления плагина в магазин вебасист, в связи с  его специфичной реализацией, т.к. другого варианта нет на данный момент.

Плагин использует подмену классов приложения, такая подмена возможна за счет хитрого метода загрузки классов приложения (waAppConfig::getClasses).
Сначала обрабатывается директория приложения shop/lib/ а затем директории плагинов plugin/lib/, что дает возможность подменить файловый адрес класса в массиве. При этом никакие файлы не изменяются и не затрагиваются, плагин просто подменяет путь файла класса для загрузки на путь из своей папки, в самом приложении все остается как есть.

Я старался как можно меньше внедряться в систему, все подменяемые классы используют обратное наследование, есть файл класса (оригинал точная копия из приложения) в папке плагина, но с префиксом плагина, его наследует подменяемый класс, и в нужных методах стоят хуки обработчики данных, которые никак не меняют логику, а просто конвертируют значения.
Также в плагине предусмотрена версионность приложения и фалов подмен плагина, что дает возможность подменять файлы без опасения не совместимости, единственный отрицательный момент, необходимо будет следить за обновлениями приложения и выпускать обновления плагина при изменении подменяемых классов в самом приложении. Частично решу динамическим копированием файлов оригиналов классов при выходе новой версии приложения, что снизит вероятность несовпадения функционала.

Данный архив, предварительная версия, для рассмотрения самой техники на возможность публикации, работа по дописанию плагина ведется...
 */
class shopReviewsplusPluginSettings {
    protected static $plugin = null;
    protected static $settings_key = array();
    protected static $settings = null;
    protected static $_change = false;
    protected static $app_settings_model = null;
    public function __construct()
    {
        if(self::$plugin === null) {
            self::$plugin = wa(shopReviewsplusPlugin::APP)->getPlugin(shopReviewsplusPlugin::PLUGIN_ID);
        }
        self::$settings_key = array(shopReviewsplusPlugin::APP, shopReviewsplusPlugin::PLUGIN_ID);
        if(self::$settings === null) {
            self::$settings = self::$plugin->getSettings();
        }
    }
    protected function getSettingsKey() {
        return self::$settings_key;
    }
    protected static function getSettingsModel()
    {
        if (!self::$app_settings_model) {
            self::$app_settings_model = new waAppSettingsModel();
        }
        return self::$app_settings_model;
    }
    public function get($name = null, $default = '') {

        if($name === null) {
            return self::$settings;
        }
        return array_key_exists($name,self::$settings)? self::$settings[$name] : $default;
    }
    public function set($name, $value) {
        self::$_change = true;
        self::$settings[$name] = $value;
    }
    public function save($data = null) {
        if($data === null && self::$_change) {
            $data = self::$settings;
        } else {
            if(!is_array($data)) {
                throw new waException('Данные настроек должны быть массивом ключ - значение!');
            }
            self::$settings = array_merge(self::$settings,   $data);
        }
        var_dump($data);
        foreach ($data as $name => $value) {
            self::getSettingsModel()->set($this->getSettingsKey(), $name, is_array($value) ? json_encode($value) : $value);
        }
        return true;
    }
    public function del($name = null) {
        return   self::getSettingsModel()->del($this->getSettingsKey(), $name);
    }
    public function __destruct()
    {
        if(self::$_change) {
            $this->save();
        }
    }

}