<?php
class shopReviewsplusPluginView {

    /**
     * @var null| waTreme
     */
    protected  $theme = null;
    /**
     * @var null|shopReviewsplusPluginTemplates
     */
    protected  $templates = null;

    protected $plugin = null;

    public function __construct()
    {
        wa('shop')->getPlugin(array('shop',));
        $this->plugin = $plugin;
    }

    /**
     * Возвращает объект шаблонов фронтенда для плагина
     * @return null|shopReviewsplusPluginTemplates
     */
    public function getTemplates() {
        if($this->templates == null) {
            $this->templates = new shopReviewsplusPluginTemplates($this->plugin);
        }
        return $this->templates;
    }

    /**
     * Метод дополняет массив или объект продукта данными плагина
     * @param $product - объект или массив продукта
     */
    public function getEntityReviews($entity) {


    }




    protected function getKey(){
        return shopReviewsplusPlugin::PLUGIN_ID;
    }

    /**
     * Возвращает новый объект представления
     * @return waSmarty3View
     */
    protected  function getView() {
        $view =  new waSmarty3View(wa());
        $wa_view = wa()->getView();
        $view->assign($wa_view->getVars());
        return $view;
    }

    /**
     * Компилирует шаблон представления и возвращает результат в виде html кода
     * @param $data -  массив данных
     * @param $template - Идентификатор шаблона в массиве шаблонов плагина
     * @return mixed|string|void
     */
    protected  function fetch($data, $template) {
        $view = $this->getView();
        $view->assign($data);
        return $view->fetch($this->getTemplates()->getTemplate($template));
    }


}