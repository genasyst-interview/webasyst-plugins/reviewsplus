<?php

/**
 * Объект витрины
 * Class shopReviewsplusPluginStorefront
 */
class shopReviewsplusPluginStorefront {
    /**
     * Название(URL) витрины
     * @var string
     */
    protected $name = '';

    /**
     * shopReviewsplusPluginStorefront constructor.
     * @param null $storefront - Название(URL) витрины
     */
    public function __construct($storefront = null)  {
        $this->setStorefront($storefront);
    }

    /**
     * Проверка и установка данных витрины
     * @param null $storefront
     */
    protected function setStorefront($storefront = null) {
       $this->name = $storefront;
    }

    /**
     * Возвращает все темы дизайна, используемые на втирине
     * @return array
     */
    public function getThemes() {
            $storefront_data = self::splitUrl($this->getName());

            if($storefront_data) {
                $routing = wa('shop')->getRouting()->getRoutes($storefront_data['domain']);
                if(empty($routing)) {
                    $routing = wa('shop')->getRouting()->getRoutes();
                }
                foreach ($routing as $route) {
                    if($route['app'] == shopReviewsplusPlugin::APP && $route['url'] == ltrim($storefront_data['url'], '/\\'))  {
                        $theme = new waTheme($route['theme'], shopReviewsplusPlugin::APP);
                        $theme_mobile = ($route['theme'] == $route['theme_mobile'])? false : new waTheme($route['theme_mobile'], shopReviewsplusPlugin::APP);
                        return array(
                            'theme' => $theme,
                            'theme_mobile' => $theme_mobile,
                        );
                    }
                }
            }
        return array();
    }
    public function getRouteUrl() {
        $storefront_data = self::splitUrl($this->getName(),true);
        return str_replace('*','',$storefront_data['url']);
    }
    /**
     * Разбирает адрес названия витрины на составляющие
     * @param $url
     * @return array|bool
     */
    public static function splitUrl($url, $root_domain = false)
    {
        if(preg_match('@^(?:http://|https://)?([^/]+)([\/].*)?@i', mb_strtolower($url), $url_arr)) {
            $domain = wa('shop')->getRouting()->getDomain($url_arr[1]);
            if(!$root_domain){
                $u = trim(wa()->getRootUrl(), '/');
                if ($u) {
                    $domain .= '/'.$u;
                }
            }
            if(count($url_arr)==3) {
                return  array(
                    'domain' => $url_arr[1],
                    'url' => (!$root_domain)? str_replace($domain,'',$url) : $url_arr[2]
                );
            }
        }
        return false;
    }
    public function getDomain() {
        if(preg_match('@^(?:http://|https://)?([^/]+)([\/].*)?@i', mb_strtolower($this->getName()), $url_arr)) {
            return  wa('shop')->getRouting()->getDomain($url_arr[1]);
        }
        return false;
    }

    /**
     * Возвращает md5 код названия витрины
     * @return string
     */
    public function getCode() {
        return md5($this->name);
    }

    /**
     * Возвращает название витрины
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * При приведении класса к строке будет выведено название витрины
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getName();
    }
}