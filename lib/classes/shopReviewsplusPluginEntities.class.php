<?php

/**
 * Class shopReviewsplusPluginReviewsEntity
 * Класс отвечает за файлы экшена
 */
class shopReviewsplusPluginEntities {
    
    protected $entity = '';
    protected $entity_id = null;
    /**
     * shopReviewsplusPluginReviewsEntity constructor.
     * @param string $entity
     * @param string|null $entity_id
     */
    public function __construct($entity = '')
    {
        if(empty($entity) || !is_string($entity)) {
            throw new waException('23171: Передан пустой идентификатор типа отзывов!');
        }
        $this->entity = (string)$entity;
    }


    public function getEntitiesFields()  {
        $fields_model = new shopReviewsplusPluginFieldsModel();
        $fields = $fields_model->getByEntity($this->entity, false);
        $entities = array();
        if(is_array($fields) && !empty($fields)) {
            foreach ($fields as $field) {
                $entities[$field['entity_id']][$field['id']] = $field; 
            }
        }
        return $entities;
    }
    public function getById($id) {
        
    }
}