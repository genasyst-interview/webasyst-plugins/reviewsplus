<?php

/**
 * Class shopReviewsplusPluginReviewsEntity
 * Класс отвечает за файлы экшена
 */
class shopReviewsplusPluginEntity {

    /**
     * @var string
     */
    protected $entity = '';
    /**
     * @var int|null
     */
    protected $entity_id = null;

    /**
     * @var null|shopReviewsplusPluginReviewsFields
     */
    protected $fields_class = null;

    public function __construct($entity = '', $entity_id = null)
    {
        if(empty($entity) || !is_string($entity)) {
            throw new waException('23171: Передан пустой идентификатор типа отзывов!');
        }
        $this->entity = (string)$entity;
        $this->entity_id = shopReviewsplusPluginHelper::castEntityId($entity_id);
        $this->fields_class = new shopReviewsplusPluginReviewsFields($this);
    }

    public function getName() {
        return $this->entity;
    }

    public function getId() {
        return $this->entity_id;
    }
    public static function create($entity = '', $entity_id = null) {
        if(($entity instanceof shopReviewsplusPluginEntity)) {
            return $entity;
        }
        return new self($entity, $entity_id);
    }

    /**
     * @return null|shopReviewsplusPluginReviewsFields
     */
    public function getFieldsClass() {
        return $this->fields_class;
    }

    /**
     * @return array|null
     */
    public function getFields() {
       return $this->fields_class->getFields();
    }
    public function getData() {
        return array(
            'entity' => $this->getName(),
            'entity_id' => (string)$this->getId()
        );
    } 
}