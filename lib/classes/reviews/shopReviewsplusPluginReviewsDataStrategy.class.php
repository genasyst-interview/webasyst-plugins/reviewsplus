<?php

/**
 * Class shopReviewsplusPluginFiles
 * Класс отвечает за файлы экшена
 */
class shopReviewsplusPluginReviewsDataStrategy {
    /**
     * @var shopReviewsplusPluginReviewsEntity
     */
    protected $entity = null;

    /**
     * @var null|shopReviewsplusPluginCustomReviewsDataModel
     */
    protected $reviews_model =  null;
    /**
     * @var null|shopReviewsplusPluginProductModeratedDataModel
     */
    protected $moderated_model =  null;

    /**
     * Костыль для моделей продукта
     * @var string
     */
    protected $moderated_state = false;

    /**
     * shopReviewsplusPluginReviewsDataStrategy constructor.
     * @param string $entity
     */
    public function __construct(shopReviewsplusPluginReviewsEntity $entity)
    {
        $this->entity = $entity;
        if($this->entity->getName() == 'product') {
            $this->reviews_model = new shopReviewsplusPluginProductReviewsDataModel($this->entity);
            $this->moderated_model = new shopReviewsplusPluginProductModeratedDataModel($this->entity);
        } else {
            $this->reviews_model = new shopReviewsplusPluginCustomReviewsDataModel($this->entity);
        }
    }

    /**
     *  Костыль для продуктов
     */
    public function setModerateState() {
        $this->moderated_state = true;
    }

    /**
     * Отмена костыля
     */
    public function setNormalState(){
        $this->moderated_state = false;
    }

    /**
     * @param int $field_id
     */
    public function deleteField($field_id = 0) {
        if($this->moderated_model instanceof shopReviewsplusPluginReviewsDataModel) {
            $this->moderated_model->deleteByFieldId($field_id);
        }
        $this->reviews_model->deleteByFieldId($field_id);
    }
    
}