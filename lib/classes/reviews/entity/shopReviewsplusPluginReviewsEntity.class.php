<?php

/**
 * Class shopReviewsplusPluginReviewsEntity
 * Класс отвечает за файлы экшена
 */
abstract class shopReviewsplusPluginReviewsEntity {


    protected $base_entity = null;

    protected $entity_id = null;
    /**
     * shopReviewsplusPluginReviewsEntity constructor.
     * @param string $entity
     * @param string|null $entity_id
     */
    public function __construct(shopReviewsplusPluginEntity $base_entity, $entity_id = null)
    {
        $this->setBaseEntity($base_entity);
        $this->setEntityId($entity_id);
    }
    protected function setBaseEntity($base_entity) {
        $this->base_entity = $base_entity;
    }
    protected function setEntityId($entity_id) {
        $this->entity_id = $entity_id;
    }
    public function getEntityId(){
        return $this->entity_id;
    }

    /**
     * @return mixed
     */
    public function getFields() {
        return $this->getBaseEntity()->getFields();
    }

    /**
     * @return shopReviewsplusPluginEntity
     */
    public function getBaseEntity() {
        return $this->base_entity;
    }
    public function getReviews($offset = 0, $count = null, $order = null) {
        
    }
    
    public function add($data, &$errors)  {
        $model = $this->getModel();
        if(!array_key_exists('status', $data)) {
            $data['status'] = shopReviewsplusPluginEntityReviewsModel::STATUS_MODERATED;
        }
        return $model->add($this->getEntityId(), $data,  $errors);
    }
    protected function getModel() {
       return new shopReviewsplusPluginReviewsModel($this->getBaseEntity());
    }
}