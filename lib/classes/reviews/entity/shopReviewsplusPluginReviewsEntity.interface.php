<?php


interface shopReviewsplusPluginReviewsEntityInterface {

    public function __construct($entity = '', $entity_id = '');
    public function addReview($data);
    public function getFields();
    public function getReviews( $offset = 0, $count = null, $order = null);
}