<?php


class shopReviewsplusPluginReviews
{
   
    protected $entity = null;

    protected $limit = 10;
    protected $settings = null;

    public function __construct($entity = '', $entity_id = null)
    {
        $this->entity = shopReviewsplusPluginReviewsEntity::create($entity, $entity_id);
    }
    public function init() {
        if($this->getEntity()->getName() == 'product') {
            $this->limit = $this->getSettings()->get('reviews_per_page');
        } elseif($this->getEntity()->getName() == 'storefront') {
            $this->limit = $this->getSettings()->get('reviews_per_page');
        } else {
            $this->limit = $this->getSettings()->get('reviews_per_page');
        }
    }
    protected function getEntity() {
        return $this->entity;
    }
    public function setEntityId($id) {
        $this->entity_id = $id;
    }
    public function get($start = 0, $limit = 10, $sort = 'DESC') {

    }
    public function getPage($page = 1,  $limit = null)  {

    }

    public static function factory($entity = '', $entity_id = null) {
        $entity =  shopReviewsplusPluginReviewsEntity::create($entity, $entity_id);
        if(shopReviewsplusPluginHelper::entityExists($entity->getName())) {
            if($entity->getName() == 'product') {
                return new shopReviewsplusPluginReviewsProduct($entity);
            } else {
                return new self($entity, $entity_id);
            }
        }
    }
    protected function getSettings() {
        if($this->settings === null) {
            $this->settings = new shopReviewsplusPluginSettings();
        }
        return $this->settings;
    }
}