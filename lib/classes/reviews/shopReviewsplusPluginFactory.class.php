<?php


class shopReviewsplusPluginFactory
{
	public function __construct()
    {

    }

    public function getEntity($entity = '', $entity_id = null) {
        if(($entity instanceof shopReviewsplusPluginEntity)) {
            return $entity;
        }
        return new shopReviewsplusPluginEntity($entity, $entity_id);
    }
    public function getReviewsEntity($entity = '',  $entity_id = null) {
        if(($entity instanceof shopReviewsplusPluginReviewsEntity)) {
            return $entity;
        }
        $entity_model = $this->getEntityModel();
        if($entity == 'product') {
            $product_model = new shopProductModel();
            $product_data = $product_model->getById($entity_id);
            if(is_array($product_data) && array_key_exists('type_id', $product_data)) {
                if($entity_model->entityExists('product', $product_data['type_id'])) {
                    $base_entity = $this->getEntity('product', $product_data['type_id']);
                } else {
                    $base_entity = $this->getEntity('product', null);
                }
            } else {
                throw new waException('Нет такого товара!');
            }
            return new shopReviewsplusPluginReviewsEntityProduct($base_entity, $entity_id);
        } else {
            $entity_data = $entity_model->getEntity($entity);
            if(!$entity_data) {
                $entity_model->createEntity($entity);
                $entity_data = $entity_model->getEntity($entity);
                $base_entity = $this->getEntity($entity);
                $base_entity->getFields()->createBaseFields();
            }
            $base_entity = $this->getEntity($entity);
            if($entity_data['type'] == 'multiple') {
                return new shopReviewsplusPluginReviewsEntityMultiple($base_entity, $entity_id);
            }  else {
                return new shopReviewsplusPluginReviewsEntitySingle($base_entity, $entity_id);
            }
        }
    }
    public function getEntityModel() {
        return new shopReviewsplusPluginEntityModel();
    }
}