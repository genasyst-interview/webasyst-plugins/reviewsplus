<?php

/**
 * Class shopReviewsplusPluginFiles
 * Класс отвечает за файлы экшена
 */
class shopReviewsplusPluginReviewsModel {
    /**
     * @var shopReviewsplusPluginEntity
     */
    protected $entity = null;
   

    /**
     * @var array
     */
    protected $models =  array();

    /**
     * Костыль для моделей продукта
     * @var string
     */
    protected $moderated_state = false;

    /**
     * shopReviewsplusPluginReviewsStrategy constructor.
     * @param string $entity
     * @param string|null $entity_id
     */
    protected $fields = array();
    public function __construct($entity = '',  $entity_id = null)
    {
        $factory = new shopReviewsplusPluginFactory();
        $this->entity = $factory->getEntity($entity, $entity_id);
        if($this->entity->getName() == 'product') {
            $this->models['review'] = new shopReviewsplusPluginProductReviewsModel();
            $this->models['review_data'] = new shopReviewsplusPluginProductReviewsDataModel($this->entity->getName());
            $this->models['moderated'] = new shopReviewsplusPluginProductModeratedModel();
            $this->models['moderated_data'] = new shopReviewsplusPluginProductModeratedDataModel($this->entity->getName());
        } else {
            $this->models['review']  = new shopReviewsplusPluginCustomReviewsModel($this->entity);
            $this->models['review_data'] = new shopReviewsplusPluginCustomReviewsDataModel($this->entity);
        }
        $this->fields = $this->getEntity()->getFields();
    }
    /* МЕТОДЫ ОТЗЫВОВ */
    public function getReviews($entity_id, $offset = 0, $count = null, $order = null) {
        $reviews_model = new shopProductReviewsModel();
        $r = $reviews_model->getReviews();
    }
    public function add($entity_id, $review,  &$errors, $options = array())  {
        if(!is_array($errors)) {
            $errors = array();
        }
        if(!$this->validate($review, $errors)) {
            return false;
        }

        if($this->isModeratedStatus($review['status']) && $this->getEntity()->getName() == 'product') {
            $model = $this->getModel('moderated');
            $data_model = $this->getModel('moderated_data');
        } else {
            $model = $this->getModel('review');
            $data_model = $this->getModel('review_data');
        }

        $review = $this->prepareReview($review);
        waLog::dump($review);
        return;
        $id = $model->add($entity_id, $review['review']);
        if(!$id) {
            $errors['all'] = 'Не удалось записать отзыв!';
            return false;
        }
        if(!$data_model->add($entity_id, $review['review_data'], $errors)) {
            return false;
        };
        return $id;

    }
    public function validate($data, &$errors) {
        foreach($this->fields as $k => $v) {
            if($v['require'] == 1 && $v['show'] == 1) {
                if(!array_key_exists($v['key'], $data) || !$this->validateValue($data[$v['key']], $v['type'])) {
                    $errors[$v['key']] = 'Неверно заполнено поле!';
                }
            }
        }
        return !empty($errors);
    }
    protected function prepareReview($data) {
        $return = array(
            'review' => array(),
            'review_data' => array()
        );
        $fields_types = shopReviewsplusPluginHelper::getFieldTypes();
        $base_fields = shopReviewsplusPluginHelper::getBaseFields();

        foreach($this->fields as $k => $v) {
            if(!array_key_exists($v['key'], $data) || $v['show'] == 0) {
                $value = $fields_types[$v['type']]['default'];
            } else {
                $value = $data[$v['key']];
            }
            if(array_key_exists($v['key'], $base_fields)) {
                $return['review'][$v['key']] = $value;
            } else {
                $return['review_data'][$v['key']] = $value;
            }
        }

        return $return;
    }
    protected function validateValue($value, $type) {
       switch ($type) {
           case 'email':
               $validator = new waEmailValidator();
               return $validator->isValid($value);
           case 'rate':
               $value = trim($value);
               return !empty(intval($value));
           default:
               $value = trim($value);
               return !empty($value);
       }
    }
    protected function isModeratedStatus($status) {
        return ($status =! shopReviewsplusPluginEntityReviewsModel::STATUS_PUBLISHED || $status =! shopReviewsplusPluginEntityReviewsModel::STATUS_DELETED);
    }
    public function saveReview() {

    }
    public function deleteReview($id) {

    }
    public function changeStatus($id, $status) {

    }
    /* МЕТОДЫ КОММЕНТАРИЕВ */
    public function getComments($review_id) {

    }
    public function addComment($review_id, $data) {

    }

    public function deleteComment($id) {

    }

    /**
     *  Костыль для продуктов
     */
    public function setModerateState() {
        $this->moderated_state = true;
    }
    
    /**
     * Отмена костыля
     */
    public function setNormalState(){
        $this->moderated_state = false;
    }

    /**
     * @param int $field_id
     */
    public function deleteFieldById($field_id = 0) {
        foreach ($this->getModels() as $model) {
            if($model instanceof shopReviewsplusPluginReviewsDataModel){
                $model->deleteByFieldId($field_id);
            }
        }
    }
    public function deleteFieldByKey($field_key = '') {
        foreach ($this->getModels() as $model) {
            if($model instanceof shopReviewsplusPluginReviewsDataModel){
                $model->deleteByFieldKey($field_key);
            }
        }
    }
    protected function getModels() {
        return $this->models;
    }
    protected function getModel($key = 'data') {
        if(array_key_exists($key,$this->models) && ($this->models[$key] instanceof waModel)) {
            return $this->models[$key];
        }
    }
    protected function getEntity() {
        return $this->entity;
    }

    
}