<?php

/**
 * Class shopReviewsplusPluginFiles
 * Класс отвечает за файлы экшена
 */
class shopReviewsplusPluginReviewsFields {
    protected $column_name_counter = 0;
    

    /* Возвращает типы полей */
    public static function getFieldTypes() {
        return shopReviewsplusPluginHelper::getFieldTypes();
    }
    public static function isFileType($type = '') {
        return shopReviewsplusPluginHelper::isFileType($type);
    }

    /**
     * @var shopReviewsplusPluginReviewsEntity
     */
    protected $entity = null;

    /**
     * Модель 
     * @var null|shopReviewsplusPluginFieldsModel
     */
    protected $fields_model = null;
    
    public function __construct($entity = '', $entity_id = null)  {
        $factory = new shopReviewsplusPluginFactory();
        $this->entity = $factory->getEntity($entity, $entity_id);
        $this->fields_model = new shopReviewsplusPluginFieldsModel();
    }
    public function getFields(/*$entity = null, $entity_id = false*/) {
       // if(is_null($entity)) {
            $entity = $this->getEntity()->getName();
            $entity_id = $this->getEntity()->getId();
            return  $this->fields_model->getByEntity($entity,  $entity_id);
       /* } else {
            if($entity_id !== false) {
                $fields = $this->fields_model->getByEntity($entity,  $entity_id);
            } else {
                $fields = $this->fields_model->getByEntity($entity,  $entity_id);
            }
            if(is_array($fields) && !empty($fields)) {
                foreach ($fields as &$v) {
                   // $this->deleteActionData($v);
                }
                return $fields;
            }
            return array();
        }*/

    }
    public function createBaseFields() {
        $fields = $this->getFields();
        if(empty($fields)) {
            $base_fields = shopReviewsplusPluginHelper::getBaseFields();
            foreach ($base_fields as $v) {
                $this->addActionData($v);
                if(!$this->getModel()->insert($v)) {
                    throw new waException('Не сохранилось базовое поле!['.$v['key'].']');
                }
            }
        }

    }
    protected function addActionData(&$data = array()) {
        if(!is_array($data)) {
            $data = array();
        }
        $data['entity']    =  $this->getEntity()->getName();
        $data['entity_id'] =  $this->getEntity()->getId();

        return $data;
    }
    protected function deleteActionData(&$data = array()) {
        if(is_array($data)) {
            unset($data['entity'], $data['entity_id']);
        }
        return $data;
    }
    public function deleteField($field_id = 0)  {
        if(!empty($field_id)) {
            $field = $this->fields_model->getById($field_id);
            if(!empty($field) && $field['delete']==1) {
                $key_fields  = $this->getModel()->getByField(array(
                    'entity' => $this->getEntity()->getName(),
                    'key' => $field['key']
                ),true);
                // Если нет данных в других подсущностях, удаляем вместе с данными
                if(count($key_fields)<2) {
                    return $this->deleteFieldWithData($field_id);
                } else {
                    return $this->getModel()->deleteById((int)$field_id);
                }

            }
        }
        return true;
    }
    public function deleteFieldWithData($field_id = 0)  {
        if(!empty($field_id)) {
            $field = $this->fields_model->getById($field_id);
            if(!empty($field)) {
                if($this->getModel()->deleteById((int)$field_id)) {
                    $entity  =  shopReviewsplusPluginReviewsEntity::create($field['entity']);
                    $reviews_data = new shopReviewsplusPluginReviewsStrategy($entity);
                    $reviews_data->deleteFieldByKey($field['key']);
                    return true;
                }
            }
        }
        return true;
    }
    public function getFieldById($field_id = 0) {
           return  $this->fields_model->getById((int)$field_id);
    }
    public function saveField($field = array())  {
        if(!is_array($field)) {
            throw new waException('Данные поля должныы быть массивом!');
        }
        if(array_key_exists('name', $field)) {
            $field['name'] = strip_tags($field['name']);
            $field['name'] = trim($field['name']);
            if(empty($field['name'])) {
                throw new waException('Имя поля не может быть пустым!');
            }
        }
        if(!array_key_exists('id',  $field)) {
            return $this->addField($field);
        }

        $id = $field['id'];
        unset($field['id']);
        $this->deleteActionData($field);
        if(!empty($id)) {
            $res = $this->fields_model->updateById($id, $field);
            if(!$res) {
                throw new waException('Не удалось обновить поле ['.$id.'] '.$field['name'].' Ошибка БД');
            }
            return $id;
        } else {
            throw new waException('Не удалось обновить поле ['.$id.'] '.$field['name'].' Ошибка БД');
        }
    }
    public function fieldKeyExists($key = '') {
        $field = $this->fields_model->getByField(array(
            'entity' => $this->getEntity()->getName(),
            'entity_id' => $this->getEntity()->getId(),
            'key' => (string)$key
        ));
        return !empty($field);
    }

    protected function addField($field = array()) {
        $errors = array();
        foreach (array('type' =>'Тип поля', 'name' => 'Имя поля', 'key' => 'Ключ поля') as $k => $v) {
            if(!array_key_exists($k, $field)) {
                $errors[] = 'Отсутствует колонка данных поля  ('.$v.') !';
            } elseif (empty($field[$k])) {
                $errors[] = $v.' не может быть пустым!';
            }
        }
        if(!empty($errors)) {
            throw new waException(implode(' ',$errors));
        }
        $types = self::getFieldTypes();
        if(!array_key_exists($field['type'],$types)) {
            throw new waException('Не удалось добавить поле, тип '.$field['type'].' не поддерживается!');
        }
        if($this->fieldKeyExists($field['key'])) {
            throw new waException('Ключ поля('.$field['key'].') уже был добавлен ранее! Измените ключ и повторите попытку!');
        }
        $field_key_data = $this->getModel()->getByField('key',$field['key']);
        if(!empty($field_key_data) && $field_key_data['type']!=$field['type']) {
            $type = $types[$field_key_data['type']];
            throw new waException('Ключ поля('.$field['key'].') уже был добавлен ранее для типа '.$type['name'].'! Измените ключ или тип поля на '.$type['name'].'!');
        }
        //$field['name_id'] = $this->generateColumnName($field['name']); /* DEPRECATED */
        $this->addActionData($field);

        $id = $this->fields_model->insert($field);
        if(!$id) {
            throw new waException('Не удалось добавить поле '.$field['name'].' Ошибка БД');
        }
        return $id;
    }


    public function generateColumnName($name) {

        $name = substr(strtolower(shopHelper::transliterate($name)), 0, 50);
        $field = $this->fields_model->getByField(array(
            'entity' => $this->getEntity()->getName(),
            'entity_id' => $this->getEntity()->getId(),
            'name_id'  => $name
        ));
        if(empty($field)) {
            return $name;
        }
        $exists = true;
        while($exists) {
            $_name = $name.'_'.$this->column_name_counter;
            $field =  $this->fields_model->getByField(array(
                    'entity' => $this->getEntity()->getName(),
                    'entity_id' => $this->getEntity()->getId(),
                    'name_id'  => $_name
            ));
            if($this->column_name_counter > 30) {
                $exists = false;
                $name .= $name.'_'.$this->column_name_counter.'_'.time();
            }

            if(!empty($field)) {
                $this->column_name_counter++;
            } else {
                $exists = false;
                $name = $_name;
            }
        }
        return $name;
    }

    protected function getEntity(){
        return $this->entity;
    }
    protected function getModel(){
        return $this->fields_model;
    }
}