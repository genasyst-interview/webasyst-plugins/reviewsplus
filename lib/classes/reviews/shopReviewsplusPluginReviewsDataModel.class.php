<?php

abstract class shopReviewsplusPluginReviewsDataModel extends waModel {

    /**
     * @var shopReviewsplusPluginReviewsEntity
     */
    protected $entity = '';
    protected $entity_column = 'entity';
    public function __construct($entity = '', $type = null, $writable = false)
    {
        $this->entity = $entity;
        parent::__construct($type, $writable);
    }
    protected function getEntity() {
        return $this->entity;
    }
    public function getByReviews($id) {
        return $this->getByField('review_id', $id, is_array($id));
    }
    protected function extendEntityData($data = array()) {
        if(!is_array($data)) {
            $data = array();
        }
        if($this->fieldExists($this->entity_column)) {
            $data['entity'] = $this->getEntity()->getName();
        }
        return $data;
    }
    public function getByEntity($id = 0, $all = true) {
        if(is_array($id)) {
            return $this->getByField($this->extendEntityData(array('entity_id' => $id)), $all);
        } else {
            return $this->getByField($this->extendEntityData(array('entity_id' => $id)));
        }
    }

    public function deleteByEntity($id = 0) {
        return $this->deleteByField($this->extendEntityData(array('entity_id' => $id)));
    }

    public function getByFieldId($id = 0, $all = true)
    {
        if(is_array($id)) {
            return $this->getByField(array('field_id' => $id),$all);
        } else {
            return $this->getByField(array('field_id' => $id));
        }
    }
    public function getByFieldKey($key = 0, $all = true)
    {
        if(is_array($key)) {
            return $this->getByField(array('field_key' => $key),$all);
        } else {
            return $this->getByField(array('field_key' => $key));
        }
    }

    public function deleteByFieldId($id = 0) {
        return $this->deleteByField(array('field_id' => $id));
    }
    public function deleteByFieldKey($key = '') {
        return $this->deleteByField(array('field_key' => $key));
    }

}