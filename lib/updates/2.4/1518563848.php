<?php
$model = new waModel();

function tableExists($table, $model)  {
    if(!empty($table)) {
        if(!$model){
            $model = new waModel();
        }

        $exists = false;
        try {
            $model->exec("SELECT 1 FROM {$table} WHERE  0");
            $exists = true;
        } catch (waDbException $e) {}

        if ($exists) {
           return true;
        }
    }
    return false;
}


function shopReviewsplusMetaRenameTemplates(){
    // переименовываем старый файл в темах
    $themes = wa()->getThemes('shop');
    $filename = 'all-reviews.html';
    $new_filename = 'plugin.reviewsplus.all-reviews.html';

    if(is_array($themes) && !empty($themes)) {
        foreach ($themes as $theme) {
            if($theme && ($theme instanceof waTheme)) {
                $file_path = $theme->getPath().'/'.$filename;
                $new_path = $theme->getPath().'/'.$new_filename;
                /* Если файл не был создан ранее, создаем */
                if (file_exists($file_path)) {
                    $content = file_get_contents($file_path);
                    if($content) {
                        $theme->addFile($new_filename, '');
                        waFiles::write($new_path,$content);
                        $theme->removeFile($filename);
                        $theme->save();
                    }
                }
            }
        }
    }
}

function shopReviewsplusMetaAddFieldsTypes(waModel $model){
    // Добавляем типы полей
    try {
        $field = $model->query("SHOW COLUMNS FROM shop_reviewsplus_fields WHERE field = 'type'")->fetchField('Type');
        if(!empty($field) && !preg_match('image', $field)) {
            $model->query("ALTER TABLE  `shop_reviewsplus_fields` CHANGE `type` `type` enum('rate','text','textarea', 'html','image','file') NOT NULL DEFAULT  'rate'");
        }
    } catch (waException $e) {
        shopReviewsplusPlugin::addLog('Не удалось добавить новые типы полей!');
        shopReviewsplusPlugin::addLog($e->getMessage());
    }
}
function shopReviewsplusMetaAddColumns(waModel $model){
    // Расширяем таблицу полей
    try {
        $model->query("SELECT entity FROM `shop_reviewsplus_fields` WHERE 0");
    } catch (waDbException $e) {
        $model->query("ALTER TABLE  `shop_reviewsplus_fields` ADD  `entity` enum('product','storefront','custom') NOT NULL DEFAULT  'product'");
    }
    try {
        $model->query("SELECT entity_id FROM `shop_reviewsplus_fields` WHERE 0 ");
    } catch (waDbException $e) {
        $model->query("ALTER TABLE  `shop_reviewsplus_fields` ADD `entity_id`  VARCHAR(255) DEFAULT NULL");
    }
}


function shopReviewsplusMetaDeleteDopTable(waModel $model){
    // Перименовываем таблицу
    try {
        $model->query("SELECT review_id FROM `shop_reviewsplus_product_reviews_data` WHERE 0");
        $model->exec("RENAME TABLE `shop_reviewsplus_product_reviews_data` TO `shop_reviewsplus_dop`");
    } catch (waDbException $e) {

    }
}


function shopReviewsplusMetaRestructureReviewsDopTable($chunk_size = 100)
{
    if ($chunk_size !== null && $chunk_size <= 0) {
        return;
    }

    $fields_model = new shopReviewsplusPluginFieldsModel();
    $fields = $fields_model->getByField('func','dop','name_id');


    if(is_array($fields) && count($fields) > 0) {

        $Dop_model = new shopReviewsplusPluginDopModel();

        $total_count = (int)$Dop_model->select('COUNT(id)')->where('review_id > 0')->fetchField();
        if($total_count > 0) {
            $fields_model->exec("CREATE TABLE IF NOT EXISTS shop_reviewsplus_product_reviews_data_temp (
              product_id int(11) UNSIGNED NOT NULL,
              review_id int(11) UNSIGNED NOT NULL,
              field_id int(11) UNSIGNED NOT NULL,
              value text NOT NULL,
              PRIMARY KEY (product_id, review_id, field_id),
              INDEX field_id (field_id),
              INDEX product_id (product_id),
              INDEX product_id_review_id (product_id, review_id),
              INDEX review_id (review_id)
            )
            ENGINE = MYISAM
            CHARACTER SET utf8
            COLLATE utf8_general_ci;");

            foreach ($fields as $field) {
                if($Dop_model->fieldExists($field['name_id'])) {
                    $sql = "  INSERT IGNORE INTO `shop_reviewsplus_product_reviews_data_temp` (`product_id`, `review_id`,`field_id`, `value`)
                      SELECT pr.product_id, dop.review_id, '{$field['id']}', dop.{$field['name_id']}
                     FROM shop_reviewsplus_dop dop 
                        LEFT JOIN shop_product_reviews pr ON pr.id = dop.review_id
                        WHERE dop.review_id > 0
                        ORDER BY dop.review_id ";
                    $Dop_model->exec($sql);
                }
            }
        }

        $total_count = (int)$Dop_model->select('COUNT(id)')->where('moderate_id > 0')->fetchField();
        if($total_count > 0) {
            $fields_model->exec("CREATE TABLE IF NOT EXISTS shop_reviewsplus_product_moderate_data_temp (
              product_id int(11) UNSIGNED NOT NULL,
              review_id int(11) UNSIGNED NOT NULL,
              field_id int(11) UNSIGNED NOT NULL,
              value text NOT NULL,
              PRIMARY KEY (product_id, review_id, field_id),
              INDEX field_id (field_id),
              INDEX product_id (product_id),
              INDEX product_id_review_id (product_id, review_id),
              INDEX review_id (review_id)
            )
            ENGINE = MYISAM
            CHARACTER SET utf8
            COLLATE utf8_general_ci;");

            foreach ($fields as $field) {
                if($Dop_model->fieldExists($field['name_id'])) {
                    $sql = "  INSERT IGNORE INTO `shop_reviewsplus_product_moderated_data_temp` (`product_id`, `review_id`,`field_id`, `value`)
                      SELECT m.product_id, dop.review_id, '{$field['id']}', dop.{$field['name_id']}
                     FROM shop_reviewsplus_dop dop 
                        LEFT JOIN shop_reviewsplus_moderated m ON m.id = dop.moderate_id
                       WHERE dop.moderate_id > 0
                       ORDER BY dop.moderate_id ";
                    $Dop_model->exec($sql);
                }
            }
        }

    }

}




try {
    $model->query("SELECT review_id FROM `shop_reviewsplus_dop` WHERE 0");
  //shopReviewsplusMetaRestructureReviewsDopTable();
  

} catch (waDbException $e) {

}

function shopReviewsplusMetaDeleteOldFiles(){
    $files = array(
        // 'shopReviewsplusPluginDopModel' => 'models/shopReviewsplusPluginDop.model.php',
        'Akismet' => 'vendors/Akismet.class.php',
    );
// Удаляем старые файлы
    $lib_path = wa()->getAppPath('plugins/reviewsplus/lib/','shop');
    foreach ($files as $k => $v) {
        if(file_exists($lib_path.$v)) {
            if(!waFiles::delete($lib_path.$v)) {
                shopReviewsplusPlugin::addLog('Не удалось удалить файл '.$k.'!');
            }
        }
    }
}
// sales_channel => backend:
/*$sql = "INSERT IGNORE INTO shop_order_params (order_id, name, value)
            SELECT o.id, 'sales_channel',  'backend:'
            FROM shop_order AS o
                LEFT JOIN shop_order_params AS op
                    ON o.id = op.order_id
                        AND op.name = 'sales_channel'
            WHERE op.order_id IS NULL";
$m->exec($sql);*/








