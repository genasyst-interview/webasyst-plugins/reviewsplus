<?php

return array(
    'name'        => 'Расширенные отзывы',
    'description' => 'Расширенные отзывы',
    'img'        => 'img/reviewsplus.png',
    'vendor'      => '986052',
    'version'     => '2.4.0',
    'shop_settings' => true,
    'custom_settings' => true, 
    'frontend' => true,    
    'handlers'    => array(
        'backend_products' => 'backendProducts',
        'frontend_header' => 'frontendHeader',
         )
);

//EOF
