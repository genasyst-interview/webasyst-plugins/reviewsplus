<?php

class shopReviewsplusPlugin extends shopPlugin
{
    const APP = 'shop';
    const PLUGIN_ID = 'reviewsplus';

    protected static $_settings = null;

    protected static $register_templates = array(
        'plugin' => array(
            'product.reviews.html' => array(
                'path' => '/templates/frontend/product.reviews.html',
                'description' => 'Файл для вывода списка отзывов'
            ),
            'product.main.html' =>  array(
                'path' => '/templates/frontend/product.main.html',
                'description' => 'Основной файл показа отзывов товара с формой'
            ),
            'all-reviews.html' => array(
                'path' => '/templates/frontend/all-reviews.html',
                'description' => 'Файл показа всех отзывов на отдельной странице'
            ),
            'shopReviewsplusPluginFrontend.css' => array(
                'path' => '/css/shopReviewsplusPluginFrontend.css',
                'description' => ''
            ),
            /* BACKEND */
            'backend_css' => array(
                'path' => '/css/shopReviewsplusPluginBackend.css',
            ),
            'backend_js' => array(
                'path' => '/js/shopReviewsplusPluginSettings.js',
            ),
        ),
        'theme' => array(
            'product.reviews.html' => array(
                'path' => 'product.reviews.html',
                'description' => 'Файл для вывода списка отзывов'
            ),
            'product.main.html' =>  array(
                'path' => 'product.main.html',
                'description' => 'Основной файл показа отзывов продукта с формой'
            ),
            'all-reviews.html' => array(
                'path' => 'all-reviews.html',
                'description' => 'Основной файл показа отзывов продукта с формой'
            ),
            'shopReviewsplusPluginFrontend.css' => array(
                'path' => 'shopReviewsplusPluginFrontend.css',
                'description' => ''
            ),
            'product.reviews.js'  =>  array(
                'path' => 'product.reviews.js',
                'description' => 'Дополнительный файл JS для переопределения поведения плагина',
                'content' => '/* НЕХРЕНА ПЕРЕОПРЕДЕЛЯТЬ JS ПЛАГИНА, ЗАКРОЙ РЕДАКТОР ТЕМЫ!!!  */',
            ),
        ),
    );
    protected static $view_class = null;
    /**
     * Проверка включенности плагина
     * @return bool
     */
    public static function isOn() {
        $info = wa(self::APP)->getConfig()->getPluginInfo(self::PLUGIN_ID);
        if(!empty($info)) {
            $status = self::getPluginSettings('state');
            return (!empty($status));
        }
        return false;
    }
    public function getRegisterTemplates() {
      return self::$register_templates;
    } 
    public function getTemplatesType() {
        return 'theme';
    }
    public function getViewClass() {
        if(self::$view_class === null) {
            self::$view_class = new shopReviewsplusPluginView($this);
        }
        return self::$view_class;
    }
    /**
     * Получение настроек плагина
     * @param string $name - all - все настройки | название настройки - только ее
     *
     * @return array - Массив настроек плагина | определенная настройка
     */
    public static function getPluginSettings($name = 'all')  {
        if(self::$_settings === null) {
            $plugin = wa(self::APP)->getPlugin(self::PLUGIN_ID);
            self::$_settings = $plugin->getSettings();
        }
        if($name === 'all') {
          return self::$_settings;
        } elseif(array_key_exists($name, self::$_settings)) {
           return self::$_settings[$name];
        }
        return null;
    }
    /**
     * Возвращает URL адрес статики
     * @return string
     */
    public static function getPluginUrlStatic($absolute = false){
        return wa(self::APP)->getAppStaticUrl(self::APP, $absolute).'plugins/'.self::PLUGIN_ID.'/';
    }

    /**
     * Вывод вкладки "Расширенные отзывы"
     * @return string
     */
    public function backendProducts() {
        $js_path = self::getPluginUrlStatic().'js/';
        $moderate = self::getPluginSettings('moderate');
        if(isset($moderate) && $moderate == 1) {
            $model = new shopReviewsplusPluginProductModeratedModel();
            $count = $model->countAll();
        } else {
            $model = new shopProductReviewsModel();
            $count = $model->countByField(array('status' => 'approved', 'depth' => 0));
        }
        $html = '<li id="s-reviewsplus">';
        $html .= '<span class="count">'.$count.'</span><a href="#/reviewsplus/"><i class="icon16"  style="background-image: url(\''.self::getPluginUrlStatic().'img/reviewsplus.png\'); background-size: 16px 16px;"></i>';
        $html .= 'Расширенные отзывы</a><script src="'.$js_path.'reviewsplus-init.js"></script></li>';
        $return['sidebar_top_li'] = $html;
        return $return;

    }


    public static function getProductReviewsHtml($product_id) {
        return self::getEntityReviewsHtml('product',  $product_id);
    }
    public static function getEntityReviewsHtml($entity, $entity_id) {
        $factory = new shopReviewsplusPluginFactory();
        try{
            $entity = $factory->getReviewsEntity($entity, $entity_id);
        } catch (waException $e) {
            return '';
        }
        $entity->get();
        


    }


    public static function sendCoupon($main = array()) {

        $settings = self::getPluginSettings();

        if(!isset($settings['discount_state']) || $settings['discount_state'] != 1){
            return array('error' => TRUE);
        }

        if(!isset($settings['coupon']) || empty($settings['coupon'])){
            self::addLog('Попытка отправить купон. Не заданы параметры купона', 'reviewsplus-error.log');
            return array('error' => TRUE);
        }

        if(!isset($main['email']) || empty($main['email'])){
            if(!isset($main['contact_id'])) {
                self::addLog('Попытка отправить купон. Нет мыла', 'reviewsplus-error.log');
                return array('error' => TRUE, 'error_msg' => 'Не введен email');
            }

            $contact = new waContact($main['contact_id']);
            $em = $contact->get('email', 'default');

            self::addLog('contact_id - '.$main['contact_id'].' email - '.$em, 'test.log');

            if(!isset($em) || empty($em)) {
                self::addLog('Попытка отправить купон. Нет мыла', 'reviewsplus-error.log');
                return array('error' => TRUE, 'error_msg' => 'Не введен email');
            }

            $main['email'] = $em;
        }

        $coupon = json_decode($settings['coupon'], true);

        if(!isset($coupon['value']) || empty($coupon['value'])) {
            self::addLog('Попытка отправить купон. Не задана скидка', 'reviewsplus-error.log');
            return array('error' => TRUE);
        }

        if(!isset($coupon['type']) || empty($coupon['type'])) {
            self::addLog('Попытка отправить купон. Не задан тип', 'reviewsplus-error.log');
            return array('error' => TRUE);
        }

        if(isset($coupon['days']) && $coupon['days'] != 0) {
            $date = strtotime("+".(int)$coupon['days']." day");
            $coupon['expire_datetime'] = date('Y-m-d H:i:s', $date);
        } else {
            $coupon['expire_datetime'] = null;
        }

        $coupon['create_datetime'] = date('Y-m-d H:i:s');
        $coupon['limit'] = 1;
        $coupon['create_contact_id'] = 0;
        $coupon['used'] = 0;

        $body = '';
        $to = '';
        $name = '';

        $comment = 'Купон за отзыв.';

        if(isset($main['coup_rev_id'])) {
            $comment .= 'Отзыв с id: '.$main['coup_rev_id'];
        }

        if(isset($main['name'])) {
            $comment .= ' От покупателя: '.htmlspecialchars($main['name']);
            $name = $main['name'];
        }

        if(isset($main['email'])) {
            $comment .= ' с email: '.htmlspecialchars($main['email']);
            $to = $main['email'];
        }

        $coupon['comment'] = $comment;

        $coupm = new shopCouponModel();

        //Получаем код с проверкой на повтор
        $code_error = TRUE;

        while($code_error) {
            $code = shopCouponsEditorAction::generateCode();
            if(!$coupm->getByField('code', $code)) {
                $code_error = FALSE;
            }
        }

        $coupon['code'] = $code;

        $tt = $coupm->insert($coupon);

        if(!$tt) {
            self::addLog('Не удалось записать купон в базу', 'reviewsplus-error.log');
            return array('error' => TRUE);
        }

        $body .= 'Уважаемый '.$name.'<br/><br/>';
        $body .= 'Спасибо за Ваш отзыв в нашем магазине.<br/>';
        $body .= 'В качестве благодарности мы высылаем Вам купон на скидку <b>'.htmlspecialchars($coupon['value']).$coupon['type'].'</b><br/><br/>';
        $body .= '<b>Код купона</b> - '.$code.'<br/>';
        if(!empty($coupon['expire_datetime'])) {
            $body .= 'Купон действует до <b>'.$coupon['expire_datetime'].'</b><br/>';
        }
        $body .= '<br/>Для получения скидки, данный код необходимо ввести при оформлении заказа<br/>';

        $shop_name = wa()->getSetting('name', '', 'shop');
        $from = wa()->getSetting('email', '', 'shop');

        if(!empty($shop_name)) {
            $body .= '<br/>С уважением, администрация интернет-магазина <a href="'.wa()->getRouteUrl('shop/frontend', array(), true).'" >'.$shop_name.'</a>';
        }


        $subject = 'Купон на скидку';

        $mail_message = new waMailMessage($subject, $body);
        $mail_message->setFrom($from, $shop_name);
        $mail_message->setTo($to);

        $ms = $mail_message->send();

	if(!$ms) {
            shopReviewsplusPlugin::addLog('Не удалось отправить купон на скидку', 'reviewsplus-error.log');
	}

        self::addLog($comment, 'reviewsplus-coupons.log');
        return array('error' => FALSE, 'msg' => ' Купон на скидку отправлен Вам на почту');
    }


    /**
     * Запись логов
     * (все файлы пишутся в /shop/plugins/reviewsplus/)
     *
     * @param string $message - текст для записи
     * @param string $file_name - имя файла
     * @return type
     */
    public static function addLog($message, $file_name = 'reviewsplus-report.log') {

        if(!class_exists('waLog')){
            return;
        }

        if(!isset($message) || empty($message)) {
            return;
        }

        $path_to_log = '/shop/plugins/reviewsplus/';

        waLog::log($message, $path_to_log.$file_name);
        return;
    }




    /* DEPRECATED */
    public static function buildReviewsList($product_id = 0, $page = 1) {

        $sett = self::getPluginSettings();
        $model = new shopProductReviewsModel();

        $reviews_count = $model->count($product_id);
        $rates = $model->getProductRates($product_id);

        if(!isset($reviews_count) || empty($reviews_count)) {
            return array('error' => true);
        }

        if(isset($sett['reviews_per_page']) && $sett['reviews_per_page'] != 0) {
            $count = $sett['reviews_per_page'];
            if($page == 1) {
                $offset = 0;
            } else {
                $offset = ($page - 1)*$count;
            }
        } else {
            $count = null;
            $offset = 0;
        }

        if(isset($sett['reviews_sort']) && !empty($sett['reviews_sort'])) {
            $order = $sett['reviews_sort']." DESC";
        } else {
            $order = null;
        }

        if(isset($sett['reviews_answers']) && $sett['reviews_answers'] == 1) {
            $reviews = $model->getFullTree($product_id, $offset, $count, $order, array('escape' => false));
        } else {
            $reviews = $model->getReviews($product_id, $offset, $count, $order, array('escape' => false));
        }

        if(!isset($reviews) && empty($reviews)) {
            return array('error' => TRUE, 'error_msg' => 'Не удалось получить отзывы');
        }

        $view = wa()->getView();

        $fields_model = new shopReviewsplusPluginFieldsModel();
        $fields = $fields_model->select('*')->fetchAll('name_id');

        if(!isset($fields) || empty($fields)) {
            $fields = array();
        }

        $view->assign('fields', $fields);

        if(wa()->getUser()->getRights('shop', 'products')) {
            $view->assign('can_del', 1);
        }

        $dop_model = new shopReviewsplusPluginDopModel();
        $reviews_ids = array_keys($reviews);
        $dop_reviews = $dop_model->getByField('review_id', $reviews_ids, 'review_id');

        foreach($dop_reviews as $rid => $dop) {
            foreach($dop as $val => $key) {
                if(array_key_exists($val, $fields)) {
                    if($fields[$val]['type'] === 'rate') {

                        $reviews[$rid]['dop_rate'][] = array('name' => $fields[$val]['name'],
                            'name_id' => $fields[$val]['name_id'],
                            'value' => $key
                        );

                    } else if($fields[$val]['type'] === 'text') {

                        $reviews[$rid]['dop_text'][] = array('name' => $fields[$val]['name'],
                            'name_id' => $fields[$val]['name_id'],
                            'value' => $key
                        );

                    } else if($fields[$val]['type'] === 'textarea') {

                        $reviews[$rid]['dop_textarea'][] = array('name' => $fields[$val]['name'],
                            'name_id' => $fields[$val]['name_id'],
                            'value' => $key
                        );

                    }
                }
            }
        }

        $view->assign('reviews', $reviews);
        $view->assign('count', $reviews_count);
        $view->assign('rates', $rates);

        $templates_path = self::getPluginPath('templates', 'frontend/reviews-list.html');
        $html = $view->fetch($templates_path['path']);

        return array('error' => FALSE, 'html' => $html);

    }
    /* DEPRECATED */
    public static function getFieldsAsType() {

        $model = new shopReviewsplusPluginFieldsModel();
        $response = array();

        $rate_fields = $model->select('*')
            ->where('type = "rate"')
            ->order('sort ASC')
            ->fetchAll('name_id');

        if($rate_fields) {
            $response['rate'] = $rate_fields;
        }

        $text_fields = $model->select('*')
            ->where('type = "text"')
            ->order('sort ASC')
            ->fetchAll('name_id');


        if($text_fields) {
            $response['text'] = $text_fields;
        }

        $textarea_fields = $model->select('*')
            ->where('type = "textarea"')
            ->order('sort ASC')
            ->fetchAll('name_id');

        if($textarea_fields) {
            $response['textarea'] = $textarea_fields;
        }

        return $response;

    }
    /**
     * DEPRECATED
     * Получение отзывов о продукте
     * @param $product_id - id продукта
     * @return string
     */
    public static function reviewsHelper($product_id = 0) {
        if($product_id === 0 || !self::isOn()) {
            return '';
        }
        $settings = self::getPluginSettings();

        $view = wa()->getView();
        /* Строка об отсутствии отзывов */
        if(isset($settings['noreviews']) && !empty($settings['noreviews'])) {
            $view->assign('noreviews',  $settings['noreviews']);
        }
        /* Счетчик количества отзывов */
        $reviews_model = new shopProductReviewsModel();
        $count = $reviews_model->countByField(array('status' => 'approved', 'depth' => 0, 'product_id' => $product_id));
        $view->assign('count', $count);
        /* Количество отзывов на страницу */
        if(isset($settings['reviews_per_page']) && $settings['reviews_per_page'] > 0) {
            $limit = $settings['reviews_per_page'];
            if($limit != 0 && ($count > $limit)) {
                $pages_count = ceil((float)$count / $limit);
                $view->assign('pages_count', $pages_count);
                $view->assign('cpage', 1);
            }
        }
        /* Отзыв в модальном окне */
        if(isset($settings['add_popup']) && $settings['add_popup'] == 1) {
            $view->assign('popup', 1);
        } else {
            $view->assign('popup', 0);
        }
        /* Передаем отзывы */
        $reviews = self::buildReviewsList($product_id);
        if($reviews['error']) {
            if(isset($reviews['error_msg'])){
                self::addLog('Продукт '.$product_id.' Ошибка: '.$reviews['error_msg'], 'reviewsplus-error.log');
            }
        } else {
            $view->assign('reviews', $reviews['html']);
        }
        /* передаем рейтинг продукта */
        $product_model = new shopProductModel();
        $rating = $product_model->select('rating')
            ->where('id = i:id', array('id' => $product_id))
            ->fetchField();
        if(isset($rating) && $rating != 0) {
            $view->assign('rating', $rating);
        }

        // Строка купона
        $discount_state = (isset($settings['discount_state']) && $settings['discount_state'] == 1) ? TRUE : FALSE;
        $discount_text = (isset($settings['discount_text']) && $settings['discount_text'] == 1) ? TRUE : FALSE;

        if($discount_state && $discount_text) {
            $coupon = array();
            if(array_key_exists('coupon', $settings)) {
                if(is_string($settings['coupon'])) {
                    $coupon = json_decode($settings['coupon'], true);
                } elseif(is_array($settings['coupon'])) {
                    $coupon = $settings['coupon'];
                }
            }

            if(isset($coupon['value']) && !empty($coupon['value'])) {
                $text = 'Добавьте отзыв и получите купон на скидку ';
                $text .= $coupon['value'].$coupon['type'];
                $view->assign('coupon_text', $text);
            }
        }



        $config = wa()->getConfig();

        $view->assign(array(
            'frontend_url' => self::getPluginPath('frontend'),
            'js_path' =>  self::getPluginPath('js'),
            'css_path' => self::getPluginPath('css'),
            'captcha_show' => (isset($settings['captcha']))? intval($settings['captcha']) : 0,
            'reply_allowed' => true,
            'auth_adapters' => $adapters = wa()->getAuthAdapters(),
            'request_captcha' => $config->getGeneralSettings('require_captcha'),
            'require_authorization' => $config->getGeneralSettings('require_authorization')
        ));

        $storage = wa()->getStorage();
        $current_auth = $storage->read('auth_user_data');
        $current_auth_source = $current_auth ? $current_auth['source'] : shopProductReviewsModel::AUTH_GUEST;
        $view->assign('current_auth_source', $current_auth_source);
        $view->assign('current_auth', $current_auth, true);

        $fields = shopReviewsplusPlugin::getFieldsAsType();

        if(isset($fields['rate'])) {
            $view->assign('rate_fields', $fields['rate']);
        }

        if(isset($fields['text'])) {
            $view->assign('text_fields', $fields['text']);
        }

        if(isset($fields['textarea'])) {
            $view->assign('textarea_fields', $fields['textarea']);
        }

        $templates_path = self::getPluginPath('templates');
        $html = $view->fetch($templates_path['path'].'frontend/reviews-main.html');
        return $html;

    }
    /* DEPRECATED */
    public static function getState() {
        return self::isOn();
    }
    /**
     *  DEPRECATED
     * Получение пути (папки) плагина
     *
     * @param string $variant - тип пути js|frontend|data|css|templates|fields
     * @param string $filename - имя файла, для проверки исправленных шаблонов
     * @return string
     *
     */
    public static function getPluginPath($variant  = '', $filename = '') {

        $path = 'plugins/reviewsplus/';
        if($variant === '') {
            return wa()->getAppStaticUrl('shop').$path;
        }
        if($variant === 'js') {
            return wa()->getAppStaticUrl('shop').$path.'js/';
        }
        if($variant === 'frontend') {
            $url = wa()->getRouteUrl('shop/frontend', array(), true);
            return $url.'reviewsplus/';
        }

        if($variant === 'fields') {
            return wa()->getAppPath($path.'lib/reviews-fields/', 'shop');
        }

        if($variant === 'templates') {
            if(!isset($filename) || empty($filename)) {
                return array('path' => wa()->getAppPath($path.'templates/', 'shop'), 'changed' => FALSE);
            }

            $temp_path = $path.'templates/'.$filename;
            $tt = wa()->getDataPath($temp_path, false, 'shop', false);

            if(file_exists($tt)){
                return array('path' => $tt, 'changed' => TRUE);
            } else {
                return array('path' => wa()->getAppPath($temp_path, 'shop'), 'changed' => FALSE);
            }
        }

        if($variant === 'css') {

            if(!isset($filename) || empty($filename)) {
                return wa()->getAppStaticUrl('shop').$path.'css/';
            }

            $temp_path = $path.'css/'.$filename;

            $tt = wa()->getDataPath($temp_path, false, 'shop', false);

            if(file_exists($tt)) {

                if($abs) {
                    return wa()->getDataUrl($temp_path, true, 'shop', false);
                }

                return array('path' => $tt, 'changed' => TRUE);

            } else {

                if($abs) {
                    return wa()->getAppStaticUrl('shop').$temp_path;
                }

                return array('path' => wa()->getAppPath($temp_path, 'shop'), 'changed' => FALSE);
            }

        }

        if(empty($variant) || !isset($variant)) {
            return wa()->getAppPath($path, 'shop');
        }
    }
    /* DEPRECATED */
    public function frontendHeader() {
        wa()->getResponse()->addJs('plugins/reviewsplus/js/reviewsplus-frontend.js',  'shop');
    }
    public function routing($route = array())
    {
        $file = $this->path.'/lib/config/routing.php';
        if (file_exists($file)) {
            /**
             * @var array $route Variable available at routing file
             */
            return include($file);
        } else {
            return array();
        }
    }

}