<?php

class shopReviewsplusPluginEntityModel extends waModel
{
    protected $table = 'shop_reviewsplus_entity';

    public function getEntity($entity = '', $entity_id = null) {
        return $this->getByField(array('entity' => $entity, 'entity_id' => $entity_id));
    }

    public function entityExists($entity = '', $entity_id) {
        return !empty($this->getEntity($entity, $entity_id));
    }
    public function createEntity($entity = '', $name = '', $type = 'single',  $entity_id = null) {
        if(!$this->entityExists($entity, $entity_id)) {
           $data = array(
               'entity' => $entity,
               'entity_id' => $entity_id,
               'type' => $type,
               'name' => $name
           );
           return $this->insert($data);
        }
        return true;
    }
}