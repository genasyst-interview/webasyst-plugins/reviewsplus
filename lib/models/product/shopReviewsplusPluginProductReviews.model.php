<?php

class shopReviewsplusPluginProductReviewsModel extends shopProductReviewsModel
{

    public function add($entity_id, $review, $parent_id = null, $before_id = null) {
        unset($review['entity_id'], $review['entity']);
        $review['product_id'] = $entity_id;

        return parent::add($review,$parent_id,$before_id);
    }
}