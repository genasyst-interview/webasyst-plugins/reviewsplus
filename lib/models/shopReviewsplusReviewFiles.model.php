<?php

/**
 * Class shopReviewsplusReviewFilesModel
 */
class shopReviewsplusReviewFilesModel extends waModel
{
    /**
     * @var string
     */
    protected $table = 'shop_advancedparams_param_file';
    /**
     * Тип экшена
     * @var null|string
     */
    protected $entity = '';

    /**
     * shopReviewsplusProductReviewFilesModel constructor.
     * @param null|string $entity
     * @param null $type
     * @param bool $writable
     */
    public function __construct($entity = '', $type = null, $writable = false)
    {
        $this->entity = $entity;
        parent::__construct($type, $writable);
    }

    /**
     * Удаляет все файлы по имени поля, используется при удалении поля
     * @param $field_name
     * @throws waException
     */
    public function deleteByName($field_name) {
        $entity_ids = $this->query('SELECT entity_id FROM '.$this->getTableName().' WHERE '.$this->getWhereByField('name', $field_name).' GROUP BY entity_id')->fetchAll('entity_id');
        foreach ($entity_ids as $entity) {
            $filesClass = new shopAdvancedparamsPluginFiles($this->entity, $entity['entity_id']);
            $filesClass->deleteFileByName($field_name);
        }
    }

    /**
     * Возвращает файл по имени поля и идентификатору экшена
     * @param $entity_id
     * @param $name
     * @return array|null
     */
    public function getByActionIdName($entity_id, $name) {
        $value = $this->getByField(array('entity' => $this->entity, 'name'=> $name, 'entity_id' => $entity_id));
        if(!empty($value)) {
            return $value;
        }
        return null;
    }

    /**
     * Удаляет файл по имени поля и идентификатору экшена
     * @param $entity_id
     * @param $name
     * @return bool
     */
    public function deleteByActionIdName($entity_id, $name) {
        return $this->deleteByField(array('entity' => $this->entity, 'entity_id' => $entity_id, 'name'=> $name));
    }

    /**
     * Возвращает все файлы экшена по его идентификатору
     * @param $entity_id
     * @return array|null
     */
    public function getByActionId($entity_id) {
        return $this->getByField(array('entity' =>$this->entity, 'entity_id'=>$entity_id),true);
    }

    /**
     * Удаляет все файлы экшена по его id
     * @param $entity_id
     */
    public function deleteByActionId($entity_id) {
        $filesClass = new shopAdvancedparamsPluginFiles($this->entity, $entity_id);
        $filesClass->deleteAll();
    }

    /**
     * Возвращает ссылку на файл от корня сайта
     * @param $entity_id
     * @param $name
     * @return string|null
     */
    public function getFileLink($entity_id, $name) {
        $file = $this->getByActionIdName($entity_id, $name);
        if($file) {
            return $file['value'];
        }
        return null;
    }

    /**
     * Сохраняет данные файла в бд или обновляет ссылку
     * @param $data
     */
    public function save($data) {
        if(!empty($data['name']) && !empty($data['entity_id'])) {
            $file = $this->getByActionIdName($data['entity_id'], $data['name']);
            if($file) {
                $this->updateByField(
                    array('entity' => $file['entity'], 'entity_id' => $file['entity_id'], 'name' => $file['entity_id']),
                    array('value' => $data['value'])
                );
            } else {
                $data['entity'] = $this->entity;
                $this->insert($data);
            }
        }
    }
}