<?php

class shopReviewsplusPluginFieldsModel extends waModel
{
    protected $table = 'shop_reviewsplus_fields';

    public function getFields()
    {
        return $this->fields;
    }
    public function getByEntity($entity = '',  $entity_id = false, $all = 'id') {
        if($entity_id === false){
            return $this->getByField(array(
                'entity' =>(string)$entity,
            ), $all);
        } else {
            return $this->getByField(array(
                'entity' =>(string)$entity,
                'entity_id' => $entity_id
            ), $all);
        }
    }
    public function deleteByEntity($entity = '',  $entity_id = null) {
        if($entity_id === false){
            return $this->deleteByField(array(
                'entity' =>(string)$entity,
            ));
        } else {
            return $this->deleteByField(array(
                'entity' =>(string)$entity,
                'entity_id' => $entity_id
            ));
        }
    }

}