<?php

class shopReviewsplusPluginBackendCommentEditController extends waJsonController {
    
    public function execute() {

        $main = waRequest::post('reviewsplus-comment', array());

        if(empty($main)) {
            $this->errors = 'Пустые данные';
            return;
        }

        if(!empty($main['parent_id'])) {
            //Если добавляем ответ
            unset($main['id']);
            $parent_id = $main['parent_id'];
            unset($main['parent_id']);

            $a_review = $this->reviews_model->getByField('id', $parent_id);

            if(!$a_review) {
                $this->errors = 'Не удалось получить родительский комментарий';
                return;
            }

            $main['product_id'] = $a_review['product_id'];
            $main['status'] = 'approved';
            $main['contact_id'] = wa()->getUser()->getId();
            $main['auth_provider'] = 'user';
            /* $main['title'] = $main['title'];
             $main['text'] = $main['text'];*/

            $tt = $this->reviews_model->add($main, $parent_id);

            if(!$tt) {
                $this->errors = 'Не удалось обновить ответ'.$tt;
                return;
            }

            $msg = 'Ответ добавлен';

        } else {
            //Если редактируем
            if(!isset($main['id']) || empty($main['id'])) {
                $this->errors = 'Не указан id';
                return;
            }

            $id = $main['id'];
            unset($main['id']);
            $tt = $this->reviews_model->updateById($id, $main);

            if(!$tt) {
                $this->errors = 'Не удалось обновить ответ';
                return;
            }

            $msg = 'Ответ отредактирован';
        }

        $this->response = $msg;

    }

}
