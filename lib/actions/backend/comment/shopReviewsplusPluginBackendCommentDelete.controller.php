<?php

class shopReviewsplusPluginBackendCommentDeleteController extends waJsonController {
    
    public function execute() {
        $comm_id = waRequest::post('comm_id', 0, 'int');

        if($comm_id === 0) {
            $this->errors = 'Не указан id комментария';
            return;
        }

        $tt = $this->reviews_model->changeStatus($comm_id, 'deleted');

        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось удалить ответ id: '.$comm_id, 'reviewsplus-error.log');
            $this->errors = 'Не удалось удалить ответ';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил ответ с id: '.$id, 'reviewsplus-report.log');
        }

        $pcount = $this->reviews_model->countByField('status', 'approved');
        $dcount = $this->reviews_model->countByField('status', 'deleted');

        $this->response = array('msg' => 'Комментарий удален', 'pcount' => $pcount, 'dcount' => $dcount);

        }

}
