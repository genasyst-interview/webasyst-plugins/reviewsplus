<?php

class shopReviewsplusPluginBackendCommentGetController extends waJsonController {
    
    public function execute() {


        $comm_id = waRequest::post('comm_id', 0, 'int');

        if($comm_id === 0) {
            $this->errors = 'Не указан id ответа';
            return;
        }

        $comm = $this->reviews_model->select('title, id, text')
            ->where('id = i:id', array('id' => $comm_id))
            ->fetchAll();

        if(!$comm) {
            $this->errors = 'Не удалось прочитать комментарий';
            return;
        }

        $this->response = $comm;

    }

}
