<?php

class shopReviewsplusPluginBackendFieldsGetTableController extends waJsonController {
    
    public function execute() {
        $fields_class = new shopReviewsplusPluginReviewsFields(shopReviewsplusPluginHelper::getPostEntity(), shopReviewsplusPluginHelper::getPostEntityId());

        $fields = $fields_class->getFields(shopReviewsplusPluginHelper::getPostEntity(),shopReviewsplusPluginHelper::getPostEntityId());
        $html = '';
        foreach($fields as $field)  {
            $html .= '<tr data-reviewsplus-fields-id="'.$field['id'].'">';

            $html .= "<td style='text-align: center;'>";
            $html .= "<a href='javascript:void(0);' class='reviewsplus-field-edit' title='Редактировать' >";
            $html .= "<i class='icon16 edit'></i></a>";
            $html .= "</td>";


            $html .= "<td>";
            if(isset($field['name_id']) && !empty($field['name_id'])) {
                $html .= $field['name_id'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";

            $html .= "<td>";
            if(isset($field['type']) && !empty($field['type'])) {
                $html .= $field['type'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";

            $html .= "<td class='reviewsplus-fields-name'>";
            if(isset($field['name']) && !empty($field['name'])) {
                $html .= $field['name'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";

            $html .= "<td style='text-align: center;'>";
            if(isset($field['required']) && $field['required'] == 1) {
                $html .= "<i class='icon16 yes'></i>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";

            $html .= "<td style='text-align: center;'>";
            if(isset($field['show']) && $field['show'] == 1) {
                $html .= "<i class='icon16 yes'></i>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";

            $html .= "<td style='text-align: center;'>";
            if(isset($field['sort'])) {
                $html .= htmlspecialchars($field['sort']);
            } else {
                $html .= 0;
            }
            $html .= "</td>";

            $html .= "<td style='text-align: center;'>";
            if(isset($field['delete']) && $field['delete']) {
                $html .= "<a href='javascript:void(0);' class='reviewsplus-field-delete' title='Удалить' >";
                $html .= "<i class='icon16 delete'></i></a>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";
        }

        $html .= "</tr>";

        $this->response['html'] = $html;
    }
}
