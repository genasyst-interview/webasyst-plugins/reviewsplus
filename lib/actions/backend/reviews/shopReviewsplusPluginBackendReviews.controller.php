<?php

class shopReviewsplusPluginBackendReviewsController extends waJsonController {
    
    public function execute() {

        $type = waRequest::post('type', '');

        if(empty($type)) {
            $this->errors = 'Не передан тип';
            return;
        }

        $page = waRequest::post('page', 1, 'int');

        $options = array();

        $limit = waRequest::cookie('reviewsplus-pp', 5, 'int');

        if($limit !== 0) {
            $options = array('limit' => $limit);
        }

        if($type === 'moderated') {

            $count = $this->moderate_model->countAll();

            if($limit != 0 && ($count > $limit)) {
                $pages_count = ceil((float)$count / $limit);
                $offset = ($page - 1) * $limit;
                $limit = $offset.','.$limit;
            }

            if($limit === 0) {
                $reviews = $this->moderate_model->order('datetime DESC')->fetchAll('id');
            } else {
                $reviews = $this->moderate_model->order('datetime DESC')
                    ->limit($limit)
                    ->fetchAll('id');
            }

        } else {

            if($type === 'published') {
                $count = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
                if($limit != 0 && ($count > $limit)) {
                    $pages_count = ceil((float)$count / $limit);
                    $offset = ($page - 1) * $limit;
                    $options['offset'] = $offset;
                }

                $options['where'] = array('status' => 'approved', 'depth' => 0);
            } elseif ($type === 'deleted') {

                $count = $this->reviews_model->countByField('status', 'deleted');
                if($limit != 0 && ($count > $limit)) {
                    $pages_count = ceil((float)$count / $limit);
                    $offset = ($page - 1) * $limit;
                    $options['offset'] = $offset;
                }

                $options['where'] = array('status' => 'deleted');
            }

            $reviews = $this->reviews_model->getList('*', $options);
        }

        if(!isset($reviews) || empty($reviews)) {
            $this->errors = 'Не удалось получить отзывы '.$type;
            return;
        }

        foreach ($reviews as $review) {
            $product_ids[] = $review['product_id'];
        }

        $reviews_ids = array_keys($reviews);

        $product_model = new shopProductModel();
        $product_ids = array_unique($product_ids);
        $products = $product_model->getByField('id', $product_ids, 'id');
        foreach ($reviews as $val => $review) {
            if (isset($products[$review['product_id']])) {
                $product = $products[$review['product_id']];
                $reviews[$val]['p_name'] = $product['name'];
            }
        }

        //Читаем доп. поля        
        if($type == 'moderated') {
            $sql_field = 'moderate_id';
        } else {
            $sql_field = 'review_id';
        }

        $dop_fields = $this->dop_model->getByField($sql_field, $reviews_ids, $sql_field);

        //Ответы
        $comments = $this->reviews_model->getByField('parent_id', $reviews_ids, 'id');

        foreach($comments as $comment) {
            unset($reviews[$comment['id']]);
            $reviews[$comment['parent_id']]['comments'][] = $comment;
        }

        //Получаем таблицу существующих полей        
        $fields = $this->fields_model->select('name, name_id, type')->fetchAll('name_id');

        if(!$fields) {
            $fields = array();
        }

        $view = wa()->getView();

        if(isset($pages_count) && $pages_count !== 0) {
            $view->assign('pages_count', $pages_count);
        }

        $view->assign('cpage', $page);
        $view->assign('fields', $fields);
        $view->assign('reviews', $reviews);
        $view->assign('dop_fields', $dop_fields);
        $view->assign('type', $type);

        $path = shopReviewsplusPlugin::getPluginPath('templates');
        $html = $view->fetch($path['path'].'backend/form.html');

        $this->response['html'] = $html;

    }
}
