<?php

class shopReviewsplusPluginBackendReviewsCartDeleteController extends waJsonController {
    
    public function execute() {

        $deleted_reviews = $this->reviews_model->select('*')
            ->where('status = "deleted"')
            ->fetchAll('id');

        if(!$deleted_reviews) {
            $this->errors = 'Нет удаленных отзывов';
            return;
        }

        $deleted_ids = array_keys($deleted_reviews);

        $tt = $this->reviews_model->deleteByField('status', 'deleted');

        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось полностью удалить все  отзывы', 'reviewsplus-error.log');
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил все отзывы', 'reviewsplus-report.log');
        }

        $this->reviews_model->repair();

        $tt_dop = $this->dop_model->deleteByField('review_id', $deleted_ids);

        if(!$tt_dop) {
            shopReviewsplusPlugin::addLog('Не удалось удалить доп. поля для всех удаляемых отзывов', 'reviewsplus-error.log');
        }

        $this->response = array('tt' => $tt, 'tt_dop' => $tt_dop);

    }
}
