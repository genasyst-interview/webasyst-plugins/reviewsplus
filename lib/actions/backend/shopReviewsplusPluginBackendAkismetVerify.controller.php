<?php

class shopReviewsplusPluginBackendAkismetVerifyController extends waJsonController {
    
    public function execute() {
        if(!class_exists('shopReviewsplusPluginAkismet')) {
            $this->errors = 'Не удалось подлючить класс akismet';
            return;
        }

        $key = waRequest::post('key', '');

        if(empty($key)) {
            $this->errors = 'Не передан ключ';
            return;
        }

        $url = urlencode(wa()->getRootUrl(true));

        $akismet = new shopReviewsplusPluginAkismet($url, $key);

        if($akismet->isKeyValid()) {
            $this->response['msg'] = 'Правильный ключ';
        } else {
            $this->errors = 'Неправильный ключ';
            return;
        }
    }
}
