<?php

class shopReviewsplusPluginBackendFieldGetController extends waJsonController {
    
    public function execute() {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);

        if(empty($id)) {
            $this->errors = 'Неправильный ID поля!';
            return;
        }

        $fields_class = new shopReviewsplusPluginReviewsFields();
        $field = $fields_class->getFieldById($id);
        if(empty($field)) {
            $this->errors = 'Поле не найдено!';
            return;
        }
        $this->response['field'] = $field;
    }
}
