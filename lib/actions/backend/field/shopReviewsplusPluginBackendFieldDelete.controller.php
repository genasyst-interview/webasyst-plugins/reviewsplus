<?php

class shopReviewsplusPluginBackendFieldDeleteController extends waJsonController {
    
    public function execute() {
        $field_id = waRequest::post('id', 0, waRequest::TYPE_INT);

        if(empty($field_id)) {
            $this->errors = 'Не задано поле';
            return;
        }
        $fields_model = new shopReviewsplusPluginFieldsModel();
        $entity = shopReviewsplusPluginHelper::getPostEntity();
        $field_data = $fields_model->getById($field_id);

        if(!empty($field_data)) {
            if($field_data['entity'] != $entity) {
                $this->errors = 'Поле не принадлежит сущности ('.$entity.')!';
                return;
            }
            $entity  = new shopReviewsplusPluginEntity($field_data['entity'], $field_data['entity_id']);
            if(!$entity->getFields()->deleteField($field_id)) {
                $this->errors = 'Не удалось удалить поле!';
            }  else {
                $this->response = $field_id;
            }
        } else {
            $this->errors = 'Поле не существует!';
        }
    }
}
