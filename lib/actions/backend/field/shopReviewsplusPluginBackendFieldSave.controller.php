<?php

class shopReviewsplusPluginBackendFieldSaveController extends waJsonController {
    
    public function execute() {
        
        $field = waRequest::post('field', array(), waRequest::TYPE_ARRAY);
        if(empty($field)) {
            $this->errors = 'Не переданы данные поля';
            return;
        }
        if(array_key_exists('id', $field) && !empty($field['id'])) {
            $fields_model = new shopReviewsplusPluginFieldsModel();
            $field_data = $fields_model->getById($field['id']);
            if(!empty($field_data)) {
                $entity  = new shopReviewsplusPluginEntity($field_data['entity'], $field_data['entity_id']);
            }
        } else {
            unset($field['id']);
            $entity = shopReviewsplusPluginHelper::getPostEntity();
            if(!shopReviewsplusPluginHelper::entityExists($entity)) {
                $this->errors = 'Экшен ('.$entity.') не поддерживается!';
                return;
            }
            $entity_id = shopReviewsplusPluginHelper::getPostEntityId();
            $entity  = new shopReviewsplusPluginEntity($entity, $entity_id);
        }
      
        try {
           $id = $entity->getFieldsClass()->saveField($field);
        } catch (waException $e){
            $this->errors[] = $e->getMessage();
            return;
        }

        $field = $entity->getFieldsClass()->getFieldById($id);
        $this->response['entity'] = $entity->getData();
        $this->response['field'] = $field;


      
    }
}
