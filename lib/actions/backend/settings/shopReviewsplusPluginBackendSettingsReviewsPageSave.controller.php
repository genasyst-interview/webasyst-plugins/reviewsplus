<?php

class shopReviewsplusPluginBackendSettingsReviewsPageSaveController extends waJsonController {
    
    public function execute() {
        $settings_class = new shopReviewsplusPluginSettings();
        $post_settings = waRequest::post('arsettings'); /* Парень не парился с названиями */
        $settings = array();
        /* Включение */
        if(isset($post_settings['state'])) {
            $settings['arstate'] =  1;
        } else {
            $settings['arstate'] =  0;
        }
       /* Количество на страницу */
        if(isset($post_settings['reviews_per_page'])){
            $settings['ar_reviews_per_page'] =  (int)$post_settings['reviews_per_page'];
        } else {
            $settings['ar_reviews_per_page'] =  0;
        }
        /* Ответы */
        if(isset($post_settings['reviews_answers'])){
            $settings['ar_reviews_answers'] =  1;
        } else { 
            $settings['ar_reviews_answers'] =  0;
        }
        /* Подождите пару лет, мы сохраняем ваши настройки на все компьютеры мира.... */
        if($settings_class->save($settings)) {
            $this->response = array('msg' => 'Сохранено');
        } else {
            $this->errors[] = 'Не удалось сохранить данные!';
        }

    }
}
