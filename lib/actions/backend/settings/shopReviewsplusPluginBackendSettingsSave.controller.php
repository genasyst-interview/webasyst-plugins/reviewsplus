<?php

class shopReviewsplusPluginBackendSettingsSaveController extends waJsonController {
    
    public function execute() {
        throw new waException('dssdf');
        $post_settings = waRequest::post('settings');
        $coupon = waRequest::post('coupon', array());
        if(isset($post_settings['discount_state'])){
            if(!isset($coupon['value']) || empty($coupon['value'])) {
                $this->errors = 'Скидка по купону не может быть пустой или нулем';
            } else {
                $coupon['value'] = (float) str_replace(',', '.', $coupon['value']);
                if(!is_numeric($coupon['value'])) {
                    $this->errors = 'Скидка по купону должна быть числом';
                } 
            }
        }
        if(!empty($this->errors)) {
            return;
        }
        // Индусский код написан под треки Ye Ishq Hai, Why This Kolaveri Di, Baamulaiza

        if(!isset($coupon['days']) || empty($coupon['days'])) {
            $coupon['days'] = 0;
        }
        if ($coupon['type'] == '%') {
            $coupon['value'] = min(max($coupon['value'], 0), 100);
        }
        if(!isset($coupon) || empty($coupon)) {
            $cur = wa()->getConfig()->getAppConfig('shop')->getOption('currency');
            $coupon = array('type' => $cur, 'value' => 0, 'days' => 0);
        }
        $post_settings['coupon'] = $coupon;

        if(isset($post_settings['discount_text'])) {
            $post_settings['discount_text'] = intval($post_settings['discount_text']);
        } else {
            $post_settings['discount_text'] = 0;
        }

        //Включение/выключение
        if(isset($post_settings['state'])){
            $post_settings['state'] = intval($post_settings['state']);
        } else {
            $post_settings['state'] = 0;
        }

        if(isset($post_settings['captcha'])){
            $post_settings['captcha'] = intval($post_settings['captcha']);
        } else {
            $post_settings['captcha'] = 0;
        }

        if(isset($post_settings['reviews_per_page'])) {
            $post_settings['reviews_per_page'] = intval($post_settings['reviews_per_page']);
        } else {
            $post_settings['reviews_per_page'] = 0;
        }

        if(isset($post_settings['reviews_answers'])){
            $post_settings['reviews_answers'] = intval($post_settings['reviews_answers']);
        } else {
            $post_settings['reviews_answers'] = 0;
        }

        if(!isset($post_settings['reviews_sort']) || empty($post_settings['reviews_sort'])){
            $post_settings['reviews_sort'] = 'datetime';
        }

        if(isset($post_settings['add_popup'])) {
            $post_settings['add_popup'] = intval($post_settings['add_popup']);
        } else {
            $post_settings['add_popup'] = 0;
        }

        if(isset($post_settings['moderate'])) {
            $post_settings['moderate'] = intval($post_settings['moderate']);
        } else {
            $post_settings['moderate'] = 0;
        }

        if(!isset($post_settings['email']) || empty($post_settings['email'])){
            $post_settings['email'] = '';
        }

        if(!isset($post_settings['noreviews'])) {
            $post_settings['noreviews'] = 0;
        }

        if(isset($post_settings['discount_state'])) {
            $post_settings['discount_state'] = intval($post_settings['discount_state']);
        } else {
            $post_settings['discount_state'] = 0;
        }
        if(isset($post_settings['akis_state'])) {
            $post_settings['akis_state'] = intval($post_settings['akis_state']);
        } else {
            $post_settings['akis_state'] = 0;
        }

        if(!isset($post_settings['akis_key']) || empty($post_settings['akis_key'])) {
            $post_settings['akis_state'] = '';
        }

        // Подождите.. <тут крутится бублик)> Сохраняем..
        $settings_class = new shopReviewsplusPluginSettings();
        $settings_class->save($post_settings);
        // Настройки магазина
        $shop = waRequest::post('shop', array());
        $app_settings_model = new waAppSettingsModel();

        if(isset($shop['authorization']) && !empty($shop['authorization'])) {
            $app_settings_model->set('shop', 'require_authorization', 1);
        } else {
            $app_settings_model->set('shop', 'require_authorization', 0);
        }

        if(isset($shop['captcha']) && !empty($shop['captcha'])) {
            $app_settings_model->set('shop', 'require_captcha', 1);
        } else {
            $app_settings_model->set('shop', 'require_captcha', 0);
        }


        if(isset($shop['coupon'])  && !empty($shop['coupon'])) {
            $app_settings_model->set('shop', 'discount_coupons', 1);
        } else {
            $app_settings_model->set('shop', 'discount_coupons', 0);
        }



        $this->response = array('msg' => 'Сохранено');
    }
}
