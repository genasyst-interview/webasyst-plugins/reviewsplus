<?php

class shopReviewsplusPluginBackendReviewRestoreController extends waJsonController {
    
    public function execute() {
        $id = waRequest::post('id', 0, 'int');

        if($id === 0) {
            $this->errors = 'Не передан id';
            return;
        }

        $tt = $this->reviews_model->changeStatus($id, 'approved');

        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось восстановить отзыв id: '.$id, 'reviewsplus-error.log');
            $this->errors = 'Не удалось восстановить отзыв';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' восстановил отзыв с id: '.$id, 'reviewsplus-report.log');
        }

        $pcount = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
        $dcount = $this->reviews_model->countByField('status', 'deleted');

        $this->response = array('msg' => 'Отзыв восстановлен', 'pcount' => $pcount, 'dcount' => $dcount);
    }

}
