<?php

class shopReviewsplusPluginBackendReviewModerateController extends waJsonController {
    
    public function execute() {
        $id = waRequest::post('id', 0, 'int');

        if($id == 0) {
            $this->errors = 'Не передан id';
            return;
        }

        $review = $this->moderate_model->getById($id);

        unset($review['id']);

        $review_id = $this->reviews_model->add($review);

        if(!$review_id) {

            shopReviewsplusPlugin::addLog('Не удалось перенести отзыв '.$id.' из moderated в основную таблицу', 'reviewsplus-error.log');
            $this->errors = 'Не удалось добавить отзыв.';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' одобрил отзыв: '.$review_id, 'reviewsplus-report.log');
        }

        $msg = 'Отзыв опубликован.';

        $email_en = (isset($review['email']) && !empty($review['email'])) ? TRUE : FALSE;
        if($email_en) {
            $review['coup_rev_id'] = $review_id;
            shopReviewsplusPlugin::sendCoupon($review);
        }


        //Переносим доп. поля с модерации
        $dop = $this->dop_model->updateByField('moderate_id', $id, array('moderate_id' => 0, 'review_id' => $review_id));

        if(!$dop) {
            shopReviewsplusPlugin::addLog('Не удалось переписать доп. поля для отзыва на модерации. id '.$review_id, 'reviewsplus-error.log');
            $msg .= 'Не удалось переписать доп. поля для отзыва на модерации.';
        }

        $mr_delete = $this->moderate_model->deleteById($id);

        if(!$mr_delete) {
            shopReviewsplusPlugin::addLog('Не удалось удалить отзыв на модерации из БД. id '.$review_id, 'reviewsplus-error.log');
            $msg .= 'Не удалось удалить отзыв на модерации из БД.';
        } else {
            $count = $this->moderate_model->countAll();
        }

        $pcount = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));

        $this->response = array('msg' => $msg, 'count' => $count, 'pcount' => $pcount);
        }

}
