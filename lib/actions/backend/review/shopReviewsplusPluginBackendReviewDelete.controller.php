<?php

class shopReviewsplusPluginBackendReviewDeleteController extends waJsonController {

    public function execute() {
        /**
         * Удаление отзыва (не полное)
         * @return type
         */

        $id = waRequest::post('id', 0, 'int');

        if($id == 0) {
            $this->errors = 'Не передан id';
            return;
        }

        $type = waRequest::post('type', '');

        if(empty($type)) {
            $this->errors = 'Не передан тип';
            return;
        }

        if($type === 'moderated') {
            //Если тип "На модерации"
            $del_review = $this->moderate_model->deleteById($id);
            $del_dop = $this->dop_model->deleteByField('moderate_id', $id);
        } elseif ($type === 'published') {
            //Если опубликованый
            $del_review = $this->reviews_model->changeStatus($id, 'deleted');
        } elseif ($type === 'deleted') {
            //Если удаленный - удаляем полностью и подчищаем доп. данные
            $del_review = $this->reviews_model->deleteById($id);
            $this->reviews_model->repair();
            $del_dop = $this->dop_model->deleteByField('review_id', $id);
        }

        if(!$del_review) {

            shopReviewsplusPlugin::addLog('Не удалось удалить отзыв. id отзыва - '.$id.' тип: '.$type, 'reviewsplus-error.log');
            $this->errors = 'Не удалось удалить отзыв';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил отзыв с id: '.$id.' тип: '.$type, 'reviewsplus-report.log');
        }


        //Считаем кол-во
        if($type === 'moderated') {
            $count = $this->moderate_model->countAll();
        } else {
            $count = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
        }

        $dcount = $this->reviews_model->countByField('status', 'deleted');

        $this->response = array('msg' => 'Отзыв удален', 'count' => $count, 'dcount' => $dcount);

    }
    public function delAllReviewsAction() {

        $deleted_reviews = $this->reviews_model->select('*')
            ->where('status = "deleted"')
            ->fetchAll('id');

        if(!$deleted_reviews) {
            $this->errors = 'Нет удаленных отзывов';
            return;
        }

        $deleted_ids = array_keys($deleted_reviews);

        $tt = $this->reviews_model->deleteByField('status', 'deleted');

        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось полностью удалить все  отзывы', 'reviewsplus-error.log');
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил все отзывы', 'reviewsplus-report.log');
        }

        $this->reviews_model->repair();

        $tt_dop = $this->dop_model->deleteByField('review_id', $deleted_ids);

        if(!$tt_dop) {
            shopReviewsplusPlugin::addLog('Не удалось удалить доп. поля для всех удаляемых отзывов', 'reviewsplus-error.log');
        }

        $this->response = array('tt' => $tt, 'tt_dop' => $tt_dop);
    }
}
