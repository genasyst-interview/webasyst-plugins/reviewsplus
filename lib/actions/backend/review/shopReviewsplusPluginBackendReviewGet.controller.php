<?php

class shopReviewsplusPluginBackendReviewGetController extends waJsonController {
    
    public function execute() {
        $id = waRequest::post('id', 0, 'int');

        if($id === 0) {
            $this->errors = 'Не передан id';
            return;
        }

        $type = waRequest::post('type', '');

        if(empty($type)) {
            $this->errors = 'Не указан тип';
            return;
        }

        //Получаем основные поля
        switch ($type) {

            case 'moderated':
                $review = $this->moderate_model->getById($id);
                break;

            case 'published':
            case 'deleted':
                $review = $this->reviews_model->getById($id);
                break;
        }

        if(!$review) {
            $this->errors = 'Не удалось получить отзыв';
            return;
        }


        //Получаем доп. поля
        if($type === 'moderated') {
            $dop = $this->dop_model->getByField('moderate_id', $id);
        } else {
            $dop = $this->dop_model->getByField('review_id', $id);
        }

        if(!$dop) {
            $dop = array();
        }

        $this->response = array('review' => $review, 'dop' => $dop);
    }

}
