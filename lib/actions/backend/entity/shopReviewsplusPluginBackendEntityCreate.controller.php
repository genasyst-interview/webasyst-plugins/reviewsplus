<?php

class shopReviewsplusPluginBackendEntityCreateController extends waJsonController {
    
    public function execute() {

        $entity = waRequest::post('entity');
        $entity_id = waRequest::post('entity_id');
        $entity_name = waRequest::post('name');

        $entity_id =  shopReviewsplusPluginHelper::castEntityId($entity_id);
        
        if(!empty($entity)) {
            $entity_model = new shopReviewsplusPluginEntityModel();
            if($entity_model->entityExists($entity, $entity_id)) {
                $this->setError('Такая сущность отзывов уже есть! ['.(string)$entity.']['.(string)$entity_id.']');
                return;
            }
            if($entity = 'product') {
                $type = 'multiple';
            } else {
                $type = 'single';
            }
           if($entity_model->createEntity($entity, $entity_name, $type, $entity_id))  {
               $entity_fields = new shopReviewsplusPluginReviewsFields($entity, $entity_id);
               try {
                   $entity_fields->createBaseFields();
                   $entity_data = array(
                       'entity' => $entity,
                       'entity_id' => (string)$entity_id,
                       'name' => htmlspecialchars($entity_name),
                       'fields' => $entity_fields->getFields()
                   );
                   $this->response['entity'] = $entity_data;
               } catch (waException $e) {
                   $this->setError($e->getMessage());
                   return;
               }
           } else {
               $this->setError('Не создалась сущность отзывов!');
           }
        }
    }
}
