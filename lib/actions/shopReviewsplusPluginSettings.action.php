<?php

class shopReviewsplusPluginSettingsAction extends waViewAction
{
    public function execute() {
        
        $js_path = shopReviewsplusPlugin::getPluginPath('js');
        $css_path = shopReviewsplusPlugin::getPluginPath('css');
       

        $plugin = wa(shopReviewsplusPlugin::APP)->getPlugin(shopReviewsplusPlugin::PLUGIN_ID);
        
        $this->view->assign('js_path', $js_path);
        $this->view->assign('css_path', $css_path);

        $this->view->assign('settings', $plugin->getSettings());
         /* Поля продуктов */
        $product_entities = new shopReviewsplusPluginEntities('product');
        $entities_fields = $product_entities->getEntitiesFields();
        $type_keys = array();
        foreach($entities_fields as $all_fields){
            foreach($all_fields as $field) {
                if($field['func'] == 'main') {
                    continue;
                }
                if(!array_key_exists($field['type'], $type_keys)) {
                    $type_keys[$field['type']] = array();
                }
                if(!array_key_exists($field['key'], $type_keys[$field['type']])) {
                    $type_keys[$field['type']][$field['key']] = array(
                        'key' => $field['key'],
                        'name' => $field['name'],
                        'type' => $field['type'],
                    );
                }
            }
        }
        $factory = new shopReviewsplusPluginFactory();
        $product_entity = $factory->getReviewsEntity('product',15);
        var_dump($product_entity->add(array(
            'sdf'=>'asd'
        )));
        $this->view->assign('type_keys', $type_keys);
        $this->view->assign('product_entities_fields', $entities_fields);
        $type_model = new shopTypeModel();
        $types = $type_model->getTypes();
        if(is_array($types) && !empty($types)) {
            $this->view->assign('product_types',  $types);
        } else {
            $this->view->assign('product_types', array());
        }
        /* Шаблоны */
        $templates = new shopReviewsplusPluginTemplates($plugin);
        $this->view->assign('plugin_templates', $templates);
        $config = wa()->getConfig();
        /* Настройки магазина */
        $this->view->assign(array(
            'request_captcha' => $config->getGeneralSettings('require_captcha'),
            'require_authorization' => $config->getGeneralSettings('require_authorization')
        ));
        
//        if(isset($settings['coupon'])) {
//            $coupon = $settings['coupon'];
//            $this->view->assign('coupon', $coupon);
//        }
    } 
    
}
