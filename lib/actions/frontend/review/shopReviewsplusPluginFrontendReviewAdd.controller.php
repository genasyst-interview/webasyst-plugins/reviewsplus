<?php


class shopReviewsplusPluginFrontendReviewAddController extends waJsonController
{
    public function execute() {
        $factory = new shopReviewsplusPluginFactory();
       try {
           $entity_data = $this->getPostEntityData();
           $entity = $factory->getReviewsEntity($entity_data['entity'], $entity_data['entity_id']);
       } catch (waException $e){
           $this->errors[] = $e->getMessage();
           return;
       }
        $errors = array();
        if(!$entity->addReview(waRequest::post(), $errors)){
            $this->errors[] = $errors;
            return;
        }
    }

    protected function getPostEntityData() {
        $data = array(
            'entity' => shopReviewsplusPluginHelper::getPostEntity(),
            'entity_id' => shopReviewsplusPluginHelper::getPostEntityId()
        );
        if(waRequest::issetPost('product_id')) {
            $data['entity'] = 'product';
            $data['entity'] = waRequest::post('product_id', 0, waRequest::TYPE_INT);
        }
        return $data;
    }

}