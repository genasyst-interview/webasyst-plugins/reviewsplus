<?php

class shopReviewsplusPluginBackendCreateFilesController extends waJsonController {
    
    public function execute() {

        /* Получаем все темы добавляем файлы плагина */
        $theme = waRequest::get('theme');
        $plugin = wa(shopReviewsplusPlugin::APP)->getPlugin(shopReviewsplusPlugin::PLUGIN_ID);

        $templates = new shopReviewsplusPluginTemplates($plugin);
        if($theme == 'all') {
           $themes = shopReviewsplusPluginTemplates::getThemes(shopReviewsplusPlugin::APP);
        } else {
            $themes = array();
            if($theme && !empty($theme)) {
                if(waTheme::exists($theme, shopReviewsplusPlugin::APP)){
                    $themes[] = new waTheme($theme, shopReviewsplusPlugin::APP);
                }
            } 
        }
        if(!empty($themes) && is_array($themes)) {
            $return = true;
            foreach ($themes as $theme) {
                if($theme && ($theme instanceof  waTheme)) {
                    if(!$templates->templatesCopyToTheme($theme)) {
                        $return = false;
                    }
                }
            }
            if(!$return) {
                $this->setError('Не удалось создать файлы в темах дизайна');
            }
        }

        
    }
}
