<?php

class shopReviewsplusPluginBackendTestsActions extends waViewActions
{


    public function defaultAction() {
        foreach (get_class_methods($this) as $method) {
            if(preg_match('/Action/i', $method)) {
                $ss = str_replace('Action','',$method);
                $url = '?plugin=reviewsplus&action=tests'.$ss;
                echo '<a href="'.$url.'">'.$ss.'</a><br>';
            }
        }

    }
    public function generateDemoDataAction() {
        $reviews_count = waRequest::get('reviews',6000, waRequest::TYPE_INT);
        $moderated_count = waRequest::get('moderated',3000, waRequest::TYPE_INT);
        $fields_model = new shopReviewsplusPluginFieldsModel();
        $reviews_model = new shopProductReviewsModel();
        $moderated_model = new shopReviewsplusPluginModeratedModel();
        $product_model = new shopProductModel();
        $Dop_model = new shopReviewsplusPluginDopModel();
        $fields = $fields_model->getByField('func', 'dop', 'name_id');
        if(count($fields)<1) {
            $this->addFieldsAction();
            $fields = $fields_model->getByField('func','dop','name_id');
        }

        $products = $product_model->getByField('status',1,'id',100);
        $test_data = array();
        for($i = 0 ; $i<count($products); $i++) {
            $test_data[] = array(
                'textarea' => $this->random_string(rand(10,1200),"all"),
                'text' =>  $this->random_string(rand(10,100),"all"),
                'rate' => rand(1,5),
                'name' => $this->random_string(rand(7,16),"upper"),
                'email' => $this->random_string(rand(3,8),"lower").'@'.$this->random_string(rand(4,8),"lower").'.ru'
            );
        }
        if($reviews_count > 10) {
            for($i = 0 ; $i<$reviews_count; $i++) {
                $test = $test_data[array_rand($test_data)];
                $review_id =  $reviews_model->add(array(
                    'product_id' => array_rand($products),
                    'parent_id' => 0,
                    'text' => $test['textarea'],
                    'title' => $test['text'],
                    'rate' => $test['rate'],
                    'datetime' => date('Y-m-d H:i:s'),
                    'status' => shopProductReviewsModel::STATUS_PUBLISHED
                ));
                $dop = array(
                    'review_id' => $review_id,
                    'moderate_id' => 0
                );
                $test = $test_data[array_rand($test_data)];
                foreach ($fields as $k => $v) {
                    if($v['type'] == 'text') {
                        $dop[$k] =  $test['text'];
                    }elseif($v['type'] == 'textarea'){
                        $dop[$k] =   $test['textarea'];
                    }elseif($v['type'] == 'rate') {
                        $dop[$k] =  $test['rate'];
                    }
                }
                $Dop_model->insert($dop);
            }
        }
        if($moderated_count > 10) {
            for($i = 0 ; $i<$reviews_count; $i++) {
                $test = $test_data[array_rand($test_data)];
                $review_id =  $moderated_model->insert(array(
                    'auth_provider' => 'user',
                    'contact_id' => wa()->getUser()->getId(),
                    'name' => wa()->getUser()->get('name').' '.$test['name'],
                    'email' => $test['email'],
                    'product_id' => array_rand($products),
                    'rate' => $test['rate'],
                    'site' => '',
                    'text' => $test['textarea'],
                    'title' => $test['text'],
                    'ip' => waRequest::getIp(true),
                    'datetime' => date('Y-m-d H:i:s'),
                    'spam' => 0
                ));
                $dop = array(
                    'review_id' => 0,
                    'moderate_id' => $review_id
                );
                $test = $test_data[array_rand($test_data)];
                foreach ($fields as $k => $v) {
                    if($v['type'] == 'text') {
                        $dop[$k] =  $test['text'];
                    }elseif($v['type'] == 'textarea'){
                        $dop[$k] =   $test['textarea'];
                    }elseif($v['type'] == 'rate') {
                        $dop[$k] =  $test['rate'];
                    }
                }
                $Dop_model->insert($dop);
            }
        }
    }
    public function addFieldsAction() {
        $fields_model = new shopReviewsplusPluginFieldsModel();
        $fiel_class = new shopReviewsplusPluginReviewsFields();

        $fields = array();
        $fields[] = array(
            'name' => 'Второй заголовок',
            'required' => 1,
            'delete' =>  1,
            'show' => 1,
            'type' => 'text',
            'func' => 'dop',
            'sort' => 0,
            'entity' => 'product',
        );
        $fields[] = array(
            'name' => 'Второй рейтинг',
            'required' => 1,
            'delete' =>  1,
            'show' => 1,
            'type' => 'rate',
            'func' => 'dop',
            'sort' => 0,
            'entity' => 'product',
        );
        $fields[] = array(
            'name' => 'Второй комментарий',
            'required' => 1,
            'delete' =>  1,
            'show' => 1,
            'type' => 'textarea',
            'func' => 'dop',
            'sort' => 0,
            'entity' => 'product',
        );
        $fields[] = array(
            'name' => 'Третий заголовок',
            'required' => 1,
            'delete' =>  1,
            'show' => 1,
            'type' => 'text',
            'func' => 'dop',
            'sort' => 0,
            'entity' => 'product',
        );
        $fields[] = array(
            'name' => 'Третий рейтинг',
            'required' => 1,
            'delete' =>  1,
            'show' => 1,
            'type' => 'rate',
            'func' => 'dop',
            'sort' => 0,
            'entity' => 'product',
        );
        $types = shopReviewsplusPluginReviewsFields::getFieldTypes();
        foreach ($fields as $field) {
            $field['name_id'] =  $fiel_class->generateColumnName($field['name']);
            $fields_model->insert($field);
            $sql = "ALTER TABLE  `shop_reviewsplus_dop` ADD `".$fields_model->escape($field['name_id'])."`  ".$types[$field['type']]['sql_type']."   DEFAULT NULL";
            $fields_model->query($sql);
        }
    }
    function random_string($length,  $chartypes)
    {
        $chartypes_array=explode(",", $chartypes);
        // задаем строки символов.
        //Здесь вы можете редактировать наборы символов при необходимости
        $lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
        $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
        $numbers = '1234567890'; // numbers
        $special = '^@*+-+%()!?'; //special characters
        $chars = "";
        // определяем на основе полученных параметров,
        //из чего будет сгенерирована наша строка.
        if (in_array('all', $chartypes_array)) {
            $chars = $lower . $upper. $numbers . $special;
        } else {
            if(in_array('lower', $chartypes_array))
                $chars = $lower;
            if(in_array('upper', $chartypes_array))
                $chars .= $upper;
            if(in_array('numbers', $chartypes_array))
                $chars .= $numbers;
            if(in_array('special', $chartypes_array))
                $chars .= $special;
        }
        // длина строки с символами
        $chars_length = strlen($chars) - 1;
        // создаем нашу строку,
        //извлекаем из строки $chars символ со случайным
        //номером от 0 до длины самой строки
        $string = $chars{rand(0, $chars_length)};
        // генерируем нашу строку
        for ($i = 1; $i < $length; $i = strlen($string)) {
            // выбираем случайный элемент из строки с допустимыми символами
            $random = $chars{rand(0, $chars_length)};
            // убеждаемся в том, что два символа не будут идти подряд
            if ($random != $string{$i - 1}) $string .= $random;
        }
        // возвращаем результат
        return $string;
    }



}
